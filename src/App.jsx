import React from 'react'
import LoginForm from './modules/auth/components/LoginForm'
import { Outlet } from 'react-router-dom'
import { Container, Row, Col } from 'react-bootstrap'
import SidebarWrapper from './layout/sidebar/components/SidebarWrapper'
import Header from './layout/header/components/Header'

const App = () => {
    return (
        <>
            {!localStorage.getItem('token') ? <LoginForm /> : (
                <Row className='g-0 vh-100' lg={2} xs={1}>
                    <Col lg={2}>
                        <SidebarWrapper />
                    </Col>
                    <Col className='vh-100 overflow-scroll bg-secondary bg-opacity-10' lg={10}>
                        <Header />
                        <Container fluid>
                            <div className='px-lg-3'>
                                <div className='content'>
                                    <Outlet />
                                </div>
                            </div>
                        </Container>
                    </Col>
                </Row>
            )}
        </>
    )
}

export default App