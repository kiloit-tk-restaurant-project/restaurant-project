import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import App from '../App'
import Roles from '../modules/role/components/Roles'
import RolePage from '../modules/role/RolePage'
import FoodCategoryPage from '../modules/foodCategory/FoodCategoryPage'
import Order from '../modules/order/components/Order'
import AddOrder from '../modules/order/components/AddOrder'
import Payment from '../modules/order/components/Payment'
import TablePage from '../modules/table/TablePage'

import FoodPage from '../modules/food/FoodPage'
import UserPage from '../modules/user/UserPage'
import OrderPage from '../modules/order/OrderPage'
import ReportPage from '../modules/report/ReportPage'
import Dashboard from '../modules/dashboard/components/Dashboard'

const AppRoutes = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<App />}>
                    <Route index element={<Dashboard />} />
                    <Route path='role/*' element={<Roles />} />
                    <Route path='table/*' element={<TablePage/>} />
                    <Route path='order/*' element={<OrderPage />} />
                    <Route path='order/add' element={<AddOrder/>} />
                    <Route path='role/*' element={<RolePage />} />
                    <Route path='order' element={<Order />} />
                    <Route path='order/payments' element={<Payment />} />
                    <Route path='user/*' element={<UserPage />} />
                    <Route path='food/*' element={<FoodPage />} />
                    <Route path='food-category/*' element={<FoodCategoryPage />} />
                    <Route path='report/*' element={<ReportPage />} />
                </Route>
            </Routes>
        </BrowserRouter>
    )
}

export default AppRoutes