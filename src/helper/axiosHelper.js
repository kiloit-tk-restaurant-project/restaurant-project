import axios from 'axios'

export const setUpAxios = () => {
    axios.defaults.baseURL = 'http://13.214.207.172:8001'
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token')
}