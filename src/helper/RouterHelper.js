const getCurrentUrl = (pathname) => {
    return  pathname.split('/')[1];
}

export const checkIsActive = (pathname, url) => {
    const current = getCurrentUrl(pathname);

    if (current === url) {
        return true;
    }

    return false;
}