import { createSlice } from "@reduxjs/toolkit";

export const sidebarSlice = createSlice({
    name: 'sidebar',
    initialState: {
        show: false,
    },
    reducers: {
        toggleSidebar: (state, action) => {
            state.show = action.payload;
        }
    }
});

export const { toggleSidebar } = sidebarSlice.actions;
export default sidebarSlice.reducer;