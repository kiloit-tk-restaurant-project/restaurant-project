import clsx from 'clsx'
import React from 'react'
import { checkIsActive } from '../../../../helper/RouterHelper'
import { Link, useLocation } from 'react-router-dom'

const SidebarItem = ({title, icon, to}) => {
    const { pathname } = useLocation();

    return (
        <li className='w-100'>
            <Link className={clsx('sidebar-link text-decoration-none p-3 d-flex gap-2 align-items-center', {'active': checkIsActive(pathname, to)})} to={'/' + to}>{icon} <span className='fs-6 mb-0'>{title}</span></Link>
        </li>
    )
}

export default SidebarItem