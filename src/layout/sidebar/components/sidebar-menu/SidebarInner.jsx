import React from 'react'
import SidebarItem from './SidebarItem'
import { MdDashboard } from 'react-icons/md'
import { FaUserCog } from 'react-icons/fa'
import { GiChoice } from 'react-icons/gi'
import { FiTable } from 'react-icons/fi'
import { FaUser } from 'react-icons/fa'
import { IoFastFood } from 'react-icons/io5'
import { BiSolidFoodMenu } from 'react-icons/bi'
import { TbReportAnalytics } from 'react-icons/tb'
import { FaAddressCard } from 'react-icons/fa'

const SidebarInnerOverview = () => {
    return (
        <>
            <SidebarItem title='Dashboard' icon={<MdDashboard />} to='' />
        </>
    )
}

const SidebarInnerAdmin = () => {
    return (
        <>
            <SidebarItem title='Role' icon={<FaUserCog />} to='role' />
            <SidebarItem title='User' icon={<FaUser />} to='user' />
        </>
    )
}

const SidebarInnerFood = () => {
    return (
        <>
            <SidebarItem title='Table' icon={<FiTable />} to='table' />
            <SidebarItem title='Food' icon={<IoFastFood />} to='food' />
            <SidebarItem title='Food Category' icon={<BiSolidFoodMenu />} to='food-category' />
            <SidebarItem title='Order' icon={<GiChoice />} to='order' />
        </>
    )
}

const SidebarInnerReport = () => {
    return (
        <>
            <SidebarItem title='Report' icon={<TbReportAnalytics />} to='report' />
            <SidebarItem title='Address' icon={<FaAddressCard />} to='address' />
        </>
    )
}

export { SidebarInnerOverview, SidebarInnerAdmin, SidebarInnerFood, SidebarInnerReport };