import { Button } from 'react-bootstrap'
import React from 'react'
import { FiLogOut } from 'react-icons/fi'
import { SidebarInnerOverview, SidebarInnerAdmin, SidebarInnerFood, SidebarInnerReport } from './sidebar-menu/SidebarInner'

const Sidebar = () => {
    const handleLogOut = () => {
        const confirm = window.confirm("Are you sure to log out?");
        if (confirm) {
            localStorage.clear();
            window.location.href = '/';
        }
    }

    return (
        <div className='overflow-scroll'>
            <ul className='list-unstyled sidebar-list w-100'>
                <>
                    <p className='fw-bold mb-0 p-3'>OVERVIEW</p>
                    <SidebarInnerOverview />
                </>
                <>
                    <p className='fw-bold mb-0 p-3'>ADMINISTRATION</p>
                    <SidebarInnerAdmin />
                </>
                <>
                    <p className='fw-bold mb-0 p-3'>FOOD</p>
                    <SidebarInnerFood />
                </>
                <>
                    <p className='fw-bold mb-0 p-3'>REPORT</p>
                    <SidebarInnerReport />
                </>
            </ul>
            <Button onClick={handleLogOut} variant='danger' className=' rounded-3 d-flex gap-3 align-items-center m-3'><FiLogOut /> Log Out</Button>
        </div>
    )
}

export default Sidebar