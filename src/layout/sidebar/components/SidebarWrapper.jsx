import React from 'react'
import Sidebar from './Sidebar'
import { Offcanvas } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { toggleSidebar } from '../core/handleOffcanvas'

const SidebarWrapper = () => {
    const { show } = useSelector(state => state.sidebar);
    const dispatch = useDispatch();

    const closeSidebar = () => dispatch(toggleSidebar(false));

    return (
        <div className='overflow-scroll position-sticky sticky-top'>
            <div className='vh-100 d-none d-lg-block mt-4'>
                <h5 className='container postion-sticky sticky-top bg-white z-1'>Restaurant Admin</h5>
                <div className='border-bottom border-secondary border-3 mt-4'></div>
                <Sidebar/>
            </div>
            <div className='d-block d-lg-none'>
                <Offcanvas show={show} onHide={closeSidebar}>
                    <Offcanvas.Header closeButton>
                        <Offcanvas.Title>Restaurant Admin</Offcanvas.Title>
                    </Offcanvas.Header>
                    <Sidebar />
                </Offcanvas>
            </div>
        </div>
    )
}

export default SidebarWrapper