const customStyle = {
    rows: {
        style: {
            fontSize: '15px',
        },
    },
    headCells: {
        style: {
            fontSize: '15px',
            fontWeight: 'bold',
        }
    }
};

export {customStyle};