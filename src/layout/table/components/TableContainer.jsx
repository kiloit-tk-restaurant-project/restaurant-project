import React, {useState} from 'react'
import DataTable from 'react-data-table-component'
import { customStyle } from '../core/customStyles'

const TableContainer = ({title, columns, data, handleRowSelected, contextActions, pagination, selectableRowDisabled, selecttableRowSelected,onSort}) => {

    const [filterText, setFilterText] = useState('');

    const filteredData = data.filter(item => {
        return item.name && item.name.toLowerCase().includes(filterText.toLowerCase());
    });
    return (

        <div>
            <input
                type="text"
                className='form-control w-25 float-end mt-4 me-4'
                placeholder="Search Filter"
                value={filterText}
                onChange={(e) => setFilterText(e.target.value)}
            />
            <DataTable
                onSort={onSort}
                title={title}
                columns={columns}
                data={filteredData} customStyles={customStyle}
                pagination={pagination}
                selectableRows
                onSelectedRowsChange={handleRowSelected}
                contextActions={contextActions}
                highlightOnHover
                selectableRowDisabled={selectableRowDisabled}
                selectableRowSelected={selecttableRowSelected}

            />
        </div>

    )
}

export default TableContainer