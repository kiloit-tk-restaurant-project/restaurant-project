import React from 'react';
import DataTable from "react-data-table-component";
import {customStyle} from "../core/customStyles";
const TableProfile = ({title,columns,data,handleRowSelected, contextActions, subHeaderComponent}) => {
    return (
        <div>
            <DataTable
                title={title}
                columns={columns}
                data={data} customStyles={customStyle}
                pagination
                onSelectedRowsChange={handleRowSelected}
                contextActions={contextActions}
                subHeader
                subHeaderComponent={subHeaderComponent}
                selectableRows
                highlightOnHover
            />
        </div>
    );
};

export default TableProfile;