import React, { useState } from 'react'
import { FaA, FaBars } from 'react-icons/fa6'
import { IoNotifications } from 'react-icons/io5'
import { useDispatch, useSelector } from 'react-redux'
import OrderItemList from '../../../modules/order/components/cart/OrderItemList'
import { FaArrowRight } from 'react-icons/fa6'
import { toggleSidebar } from '../../sidebar/core/handleOffcanvas'

const Header = () => {
    const [show, setShow] = useState(false);
    const { orderItems } = useSelector(state => state.order);
    const dispatch = useDispatch();

    const handleShow = () => setShow(true);
    const handleHide = () => setShow(false);
    const showSidebar = () => dispatch(toggleSidebar(true));

    return (
        <React.Fragment>
            <header className='position-sticky sticky-top bg-secondary-subtle bg-opacity-75 d-flex justify-content-between align-items-center px-3 py-2 mb-4'>
                <FaBars onClick={showSidebar} className='d-block d-lg-none cursor-pointer fs-4 text-dark' />
                <FaArrowRight className='layout-arrow d-none d-lg-block cursor-pointer fs-4 text-dark' />
                <div className='d-flex gap-4 justify-content-end align-items-center'>
                    <div className='position-relative'>
                        <IoNotifications onClick={orderItems.length == 0 ? undefined : handleShow} className='cursor-pointer fs-3 text-dark' />
                        <span className='position-absolute top-0 start-100 translate-middle text-primary badge'>{orderItems.length || ''}</span>
                    </div>
                    <p className='mb-0'>{JSON.parse(localStorage.getItem('scopeData'))?.username}</p>
                    <img className='header-admin-logo rounded-circle' src='https://ps.w.org/user-avatar-reloaded/assets/icon-256x256.png?rev=2540745' alt='' />
                </div>
            </header>
            <OrderItemList show={show} onHide={handleHide} />
        </React.Fragment>
    )
}

export default Header