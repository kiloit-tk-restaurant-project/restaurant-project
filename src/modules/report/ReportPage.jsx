import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import Report from './components/Report'

const ReportPage = () => {
    return (
        <Routes>
            <Route index path='food' element={<Report />} />
            <Route path='sale/income' element={<Report />} />
            <Route path='sale/staff' element={<Report />} />
            <Route path='*' element={<Navigate to='food' />} />
        </Routes>
    )
}

export default ReportPage