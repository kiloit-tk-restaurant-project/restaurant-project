import { createSlice } from '@reduxjs/toolkit'

export const reportSlice = createSlice({
    name: 'report',
    initialState: {
        food: [],
        income: [],
        staff: [],
    },
    reducers: {
        fetchFoodReport: (state, action) => {
            state.food = action.payload;
        },
        fetchIncomeReport: (state, action) => {
            state.income = action.payload;
        },
        fetchStaffReport: (state, action) => {
            state.staff = action.payload;
        }
    }
});

export const { fetchFoodReport, fetchIncomeReport, fetchStaffReport } = reportSlice.actions;
export default reportSlice.reducer;