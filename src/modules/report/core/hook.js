import { useDispatch } from 'react-redux'
import { foodReportGetReq, incomeReportGetReq, staffReportGetReq } from './request'
import { fetchFoodReport, fetchIncomeReport, fetchStaffReport } from './reportSlice'

const useReport = () => {
    const dispatch = useDispatch();

    const getFoodReport = () => {
        return foodReportGetReq().then(res => dispatch(fetchFoodReport(res.data.data))).catch(err => console.log(err));
    }
    const getIncomeReport = () => {
        return incomeReportGetReq().then(res => dispatch(fetchIncomeReport(res.data.data))).catch(err => console.log(err));
    }
    const getStaffReport = () => {
        return staffReportGetReq().then(res => dispatch(fetchStaffReport(res.data.data))).catch(err => console.log(err));
    }

    return { getFoodReport, getIncomeReport, getStaffReport }
}

export { useReport };