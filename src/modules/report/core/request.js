import axios from 'axios'

const foodReportGetReq = () => {
    return axios.get('/report/food');
}
const incomeReportGetReq = () => {
    return axios.get('/report/income?month=2024:05');
}
const staffReportGetReq = () => {
    return axios.get('/report/staff');
}

export { foodReportGetReq, incomeReportGetReq, staffReportGetReq };