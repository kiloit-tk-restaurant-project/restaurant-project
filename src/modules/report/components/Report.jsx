import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useReport } from '../core/hook'
import TableContainer from '../../../layout/table/components/TableContainer'
import { foodColumns, incomeColumns, staffColumns } from './table/column'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import clsx from 'clsx'
import { Form } from 'react-bootstrap'

const Report = () => {
    const { food, income, staff } = useSelector(state => state.report);
    const { getFoodReport, getIncomeReport, getStaffReport } = useReport();
    const navigate = useNavigate();

    useEffect(() => {
        getFoodReport();
        getIncomeReport();
        getStaffReport();
    }, []);

    const { pathname } = useLocation();
    const endpoint = pathname.split('/')[2];

    const handleChangeSalePath = e => window.location.href = `/report/sale/${e.target.value}`;

    return (
        <React.Fragment>
            <div className='d-flex gap-3 mb-4'>
                <Link className={clsx('text-dark text-decoration-none border-primary border-3', {'border-bottom': endpoint === 'food'})} to='/report/food'>Food</Link>
                <a onClick={() => window.location.href = '/report/sale/income'} className={clsx('text-dark text-decoration-none border-primary border-3', {'border-bottom': endpoint === 'sale'})} role='button'>Sale</a>
            </div>
            {endpoint === 'sale' ? (
                <Form.Select defaultValue={pathname.split('/')[3]} onChange={handleChangeSalePath} className='rounded-3 w-auto mb-4'>
                    <option value='income'>Income</option>
                    <option value='staff'>Staff Sold</option>
                </Form.Select>
            ) : null}
            <div className='bg-white mb-4 rounded-4 overflow-hidden'>
                <TableContainer columns={endpoint === 'food' ? foodColumns : (pathname.split('/')[3] === 'income' ? incomeColumns : staffColumns)} data={endpoint === 'food' ? food : (pathname.split('/')[3] === 'income' ? income : staff)} pagination={true} />
            </div>
        </React.Fragment>
    )
}

export default Report