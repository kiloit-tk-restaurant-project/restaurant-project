const foodColumns = [
    {
        name: 'Food Name',
        selector: row => row.food.name,
    },
    {
        name: 'Code',
        selector: row => row.food.code,
    },
    {
        name: 'Total Price',
        selector: row => `$ ${row.totalPrice.toFixed(2)}`,
    },
    {
        name: 'Sole Out',
        selector: row => row.totalQuantitySold,
    },
    {
        name: 'Unit Price',
        selector: row => `$ ${row.unitPrice.toFixed(2)}`,
    },
];

const incomeColumns = [
    {
        name: 'Income Date',
        selector: row => row.incomeDate,
    },
    {
        name: 'Payment Method',
        selector: row => row.paymentMethod,
    },
    {
        name: 'Total Price',
        selector: row => `$ ${row.totalPrice.toFixed(2)}`,
    },
];

const staffColumns = [
    {
        name: 'Cashier',
        selector: row => row.cashier.name,
    },
    {
        name: 'Payment Mehod',
        selector: row => row.paymentMethod,
    },
    {
        name: 'Total Price',
        selector: row => row.totalPrice,
    },
];

export { foodColumns, incomeColumns, staffColumns };