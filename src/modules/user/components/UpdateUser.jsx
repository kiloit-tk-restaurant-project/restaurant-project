import React, {useEffect, useState} from 'react';
import {Link, useParams} from "react-router-dom";
import {useSelector} from "react-redux";
import {useUser} from "../core/hook";

const UpdateUser = () => {
    const [onUpdateUser, setOnUpdateUser] = useState(
        {
            name: '',
            username: '',
            gender: '',
            phone: '',
            avatar: '',
            role_id: 1,
        }
    );

    const {id} = useParams();
    const {list} = useSelector(state => state.user)
    const {getUsers, onUpdate} = useUser();

    useEffect(() => {
        getUsers();
        const viewUser = list.find(user => {
            return user.id.toString() === id;
        });
        const {name, username, gender, phone, avatar} = viewUser;
        setOnUpdateUser({
            ...onUpdateUser,
            name,
            username,   
            gender,
            phone,
            avatar,
        });
    }, []);

    const handleChange = e => {
        const {name, value} = e.target;
        setOnUpdateUser({...onUpdateUser, [name]: value});
    }

    const handleSave = e => {
        e.preventDefault();
        onUpdate(id, onUpdateUser);
    }

    return (
        <div>
            <div>
                <Link to='/user' className='btn btn-secondary'>Back</Link>
            </div>
            <div className='d-flex justify-content-center border-0 rounded-5 bg-light p-5 w-75 m-auto '>
                <form>
                    <p className='text-center fs-1 fw-bold mt-2'>Update User</p>
                    <div className='row'>
                        <div className='col-lg-6 col-md-12'>
                            <div className=''>
                                <label htmlFor="username">Username</label>
                                <input name='username' onChange={handleChange} id='username' type="text"
                                       value={onUpdateUser.username} className='form-control w-100 my-2 m-auto py-3'/>
                            </div>
                            <div>
                                <label htmlFor="name">Name</label>
                                <input name='name' onChange={handleChange} id='name' type="text"
                                       value={onUpdateUser.name}
                                       className='form-control w-100 my-2 m-auto py-3'/>
                            </div>
                            <div>
                                <label htmlFor="gender">Gender</label>
                                <input name='gender' onChange={handleChange} id='gender' type="text"
                                       value={onUpdateUser.gender}
                                       className='form-control w-100 my-2 m-auto py-3' l/>
                            </div>
                        </div>
                        <div className='col-lg-6 col-md-12'>
                            <div>
                                <label htmlFor="phone">Phone</label>
                                <input name='phone' onChange={handleChange} id='phone' type="text"
                                       value={onUpdateUser.phone}
                                       className='form-control w-100 my-2 m-auto py-3'/>
                            </div>
                            <div>
                                <label htmlFor="avatar">Avatar</label>
                                <input name='avatar' onChange={handleChange} id='avatar' type="text"
                                       value={onUpdateUser.avatar}
                                       className='form-control w-100 my-2 m-auto py-3'/>
                            </div>
                            <div>
                                <label htmlFor="roleId">Role ID</label>
                                <input name='role_id' onChange={handleChange} id='roleId' type="number" value={1}
                                       className='form-control w-100 my-2 m-auto py-3'/>
                            </div>
                        </div>
                    </div>
                    <button className='btn btn-primary' type='submit' onClick={handleSave}>Save</button>
                </form>
            </div>
        </div>

    );
};

export default UpdateUser;