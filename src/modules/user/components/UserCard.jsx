// src/UserCard.js
import React, {useEffect, useState} from 'react';
import { Card, Row, Col, Image, Container } from 'react-bootstrap';
import '../../../assets/css/components/UserCard.css';
import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";


const UserCard = () => {
    const { id } = useParams();
    const users = useSelector(state => state.user.list);
    const [user, setUser] = useState({});
    console.log(users);


    useEffect(() => {
        setUser(users.find(user => user.id === Number(id)));
    }, []);

    return (
        <div className="align-self-center ">
            <h5 className='mb-5'>User Profile</h5>
            <Row className="justify-content-center ">
                <Col xl={6} md={12}>
                    <Card className="user-card-full">
                        <Row className="no-gutters">
                            <Col sm={4} className="bg-c-lite-green user-profile">
                                <Card.Body className="text-center text-white">
                                    <div className="m-b-25">
                                        <Image src="https://img.icons8.com/bubbles/100/000000/user.png" roundedCircle alt="User-Profile-Image" />
                                    </div>
                                    <h6 className="f-w-600">Hembo Tingor</h6>
                                    <p>{user.name}</p>
                                    <i className="mdi mdi-square-edit-outline feather icon-edit m-t-10 f-16"></i>
                                </Card.Body>
                            </Col>
                            <Col sm={8}>
                                <Card.Body>
                                    <h6 className="m-b-20 p-b-5 b-b-default f-w-600">Information</h6>
                                    <Row>
                                        <Col sm={6}>
                                            <p className="m-b-10 f-w-600">Email</p>
                                            <h6 className="text-muted f-w-400">rntng@gmail.com</h6>
                                        </Col>
                                        <Col sm={6}>
                                            <p className="m-b-10 f-w-600">Phone</p>
                                            <h6 className="text-muted f-w-400">98979989898</h6>
                                        </Col>
                                    </Row>
                                    <h6 className="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">Projects</h6>
                                    <Row>
                                        <Col sm={6}>
                                            <p className="m-b-10 f-w-600">Recent</p>
                                            <h6 className="text-muted f-w-400">Sam Disuja</h6>
                                        </Col>
                                        <Col sm={6}>
                                            <p className="m-b-10 f-w-600">Most Viewed</p>
                                            <h6 className="text-muted f-w-400">Dinoter husainm</h6>
                                        </Col>
                                    </Row>
                                    <ul className="social-link list-unstyled m-t-40 m-b-10">
                                        <li><a href="#!" data-toggle="tooltip" title="facebook"><i className="mdi mdi-facebook feather icon-facebook facebook"></i></a></li>
                                        <li><a href="#!" data-toggle="tooltip" title="twitter"><i className="mdi mdi-twitter feather icon-twitter twitter"></i></a></li>
                                        <li><a href="#!" data-toggle="tooltip" title="instagram"><i className="mdi mdi-instagram feather icon-instagram instagram"></i></a></li>
                                    </ul>
                                </Card.Body>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
        </div>
    );
};

export default UserCard;
