import React from 'react'
import { Image } from 'react-bootstrap'

const FoodImage = ({src}) => {
    return (
        <div className='py-2'>
            <Image className='food-img' src={src} alt='' />
        </div>
    )
}

export default FoodImage