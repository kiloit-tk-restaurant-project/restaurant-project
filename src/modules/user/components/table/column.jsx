import ActionCell from './ActionCell'
import FoodImage from './FoodImage'
import StatusSwitcher from "./StatusSwitcher";

const tempAvatar = 'https://media.istockphoto.com/id/1223671392/vector/default-profile-picture-avatar-photo-placeholder-vector-illustration.jpg?s=612x612&w=0&k=20&c=s0aTdmT5aU6b8ot7VKm11DeID6NctRCpB755rA1BIP0=';
const columns = [
    {
        name: 'Image',
        selector: row => row.avatar,
        cell: row => <FoodImage src={row.avatar || tempAvatar} />
    },
    {
        name: 'Name',
        selector: row => row.name,
        sortable:true,
        sortField: 'name',
    },
    {
        name: 'Username',
        selector: row => row.username,
        sortable:true,
        sortField: 'username',
    },
    {
        name: 'Role',
        selector: row => row.role.name,
    },
    {
      name:'Salary',
      selector:row => row.salary,
        sortable:true,
        sortField: 'salary',
    },
    {
        name: 'Status',
        cell: row => <StatusSwitcher row={row} />
    },
    {
        name: 'Action',
        cell: row => <ActionCell id={row.id} />
    },
];

export { columns };