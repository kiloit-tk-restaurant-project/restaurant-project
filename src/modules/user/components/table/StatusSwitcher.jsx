import React from 'react';
import {Form} from "react-bootstrap";
import {useUser} from "../../core/hook";

const StatusSwitcher = ({row}) => {
    const { updateStatus } = useUser();

    const handleUpdateStatus = (id) => updateStatus(id);

    return (
        <div>
            <Form.Switch defaultChecked={row.status} onChange={() => handleUpdateStatus(row.id )} />
        </div>
    );
};

export default StatusSwitcher;