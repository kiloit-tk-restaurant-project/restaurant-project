import React, {useContext, useState} from 'react'
import { IoMdMore } from 'react-icons/io'
import { FaEdit } from 'react-icons/fa'
import { FaEye } from 'react-icons/fa'
import { useNavigate } from 'react-router-dom'
import { useAuth } from '../../../auth/core/hook'

import user, {UserContext} from "../User";
import {useDispatch, useSelector} from "react-redux";

import {changeUserUpdate} from "../../core/userSlice";


const ActionCell = ({id}) => {
    const dispatch=useDispatch
    const setId = useContext(UserContext);
    const users=useSelector(state=>state.user)
    const navigate = useNavigate();
    const { checkScope } = useAuth();
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = (id) => {
        setShow(true);
        setId(id);
    };

    return (
        <div className='dropdown'>
            <span className='dropdown-toggler d-flex justify-content-center align-items-center' data-bs-toggle='dropdown'>
                <IoMdMore className='fs-5' />
            </span>
            <ul className='dropdown-menu'>
                <li>
                    <div>
                    {/*<a onClick={() => handleShow(id)} className='dropdown-item' role='button'><FaEye className='me-2 mb-1' />View details</a>*/}
                    {/*    <Offcanvas show={show} onHide={handleClose} placement="end">*/}
                    {/*        <Offcanvas.Header closeButton>*/}
                    {/*            <Offcanvas.Title>User Profile</Offcanvas.Title>*/}
                    {/*        </Offcanvas.Header>*/}
                    {/*        <Offcanvas.Body>*/}
                    {/*            /!*<ViewUser/>*!/*/}
                    {/*            <UserProfile/>*/}

                    {/*        </Offcanvas.Body>*/}
                    {/*    </Offcanvas>*/}
                        <a onClick={() => navigate(`/user/profile/${id}`)} className='dropdown-item' role='button'><FaEye className='me-2 mb-1' />View details</a>
                    </div>
                </li>
                {checkScope('edit-user') ? (
                    <li>
                        <a onClick={() => navigate(`/user/update/${id}`)}  className='dropdown-item' role='button'><FaEdit className='me-2 mb-1' />Update</a>
                    </li>
                ) : null}
            </ul>
        </div>
    )
}

export default ActionCell