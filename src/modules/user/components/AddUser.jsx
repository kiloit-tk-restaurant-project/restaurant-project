import React, {useEffect, useRef, useState} from 'react'
import {Button, Card, Col, Form, Image, Row} from 'react-bootstrap'
import {useDispatch, useSelector} from 'react-redux'
import {useRole} from '../../role/core/hook'
import {clearUserFields, setUser} from '../core/userSlice'
import {useUser} from '../core/hook'
import {useParams} from 'react-router-dom'
import '../../../assets/css/components/card.css'

const AddUser = () => {
    const inputRef = useRef(null)
    const {id} = useParams();
    const roles = useSelector(state => state.role.list);
    const {user,profileUrl} = useSelector(state => state.user);
    const {getRoles} = useRole();
    const dispatch = useDispatch();
    const {postUser, getUserById, putUser, uploadProfile,setProfile} = useUser();
    const noneAvatar = "https://media.istockphoto.com/id/1223671392/vector/default-profile-picture-avatar-photo-placeholder-vector-illustration.jpg?s=612x612&w=0&k=20&c=s0aTdmT5aU6b8ot7VKm11DeID6NctRCpB755rA1BIP0=";
    // const [profileImageUrl, setProfileImageUrl] = useState(null);

    const handleChange = e => {
        dispatch(setUser({[e.target.name]: e.target.value}));
    }

    const handleClear = () => dispatch(clearUserFields());

    const handleUpload = e => {
        e.preventDefault();
        const file = e.target.files[0];
        if (file) {
            const userForm = new FormData();
            userForm.append('file', file);
            uploadProfile(id, userForm);
            setProfile(URL.createObjectURL(file));
        }
    }

    const handleSubmit = e => {
        e.preventDefault();
        const {name, username, gender, email, password, confirm_password, phone, salary, hire_date, role_id} = user;

        if (id) {
            putUser(id, {
                name,
                username,
                gender,
                email,
                phone,
                role_id: Number(role_id)
            });
        } else {
            postUser({
                name,
                username,
                gender,
                email,
                password,
                confirm_password,
                phone,
                salary: Number(salary),
                hire_date: hire_date !== "" ? hire_date + ':00Z' : "",
                role_id: Number(role_id)
            });
        }
    }

    useEffect(() => {
        getRoles();
    }, []);

    useEffect(() => {
        if (id) getUserById(id);
        else handleClear();
    }, [id]);

    const {avatar} = user;
    return (
        <React.Fragment>
            <h3 className='mb-3'>{id ? 'Update' : 'New'} User</h3>
            <div className='p-4 pb-5 rounded-4 bg-white'>
                <Form>
                    <Row className="no-gutters">
                        <Col sm={3}>
                            <Card.Body className="text-white">
                                <div className='d-block'>
                                    <img src={profileUrl || avatar || noneAvatar} alt='User Profile' className='user-img' />
                                    <input type='file' onChange={handleUpload} ref={inputRef} className='d-none' id='upload' />
                                    <label htmlFor='upload' className='d-block btn btn-sm btn-primary w-25 mt-2 mb-2'>Upload</label>
                                </div>
                            </Card.Body>
                        </Col>
                    </Row>
                    <Row className='g-3' xs={1} sm={2}>
                        <Col>
                            <Form.Group controlId='name'>
                                <Form.Label className='mb-1'>Name</Form.Label>
                                <Form.Control value={user.name} onChange={handleChange} name='name' className='rounded-3' type='text' />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId='username'>
                                <Form.Label className='mb-1'>Username</Form.Label>
                                <Form.Control value={user.username} onChange={handleChange} name='username' className='rounded-3' type='text' />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId='gender'>
                                <Form.Label className='mb-1'>Gender</Form.Label>
                                <Form.Select value={user.gender} onChange={handleChange} name='gender' className='rounded-3'>
                                    <option value='Male'>Male</option>
                                    <option value='Female'>Female</option>
                                </Form.Select>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId='email'>
                                <Form.Label className='mb-1'>Email</Form.Label>
                                <Form.Control value={user.email} onChange={handleChange} name='email' className='rounded-3' type='email' />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId='phone'>
                                <Form.Label className='mb-1'>Phone Number</Form.Label>
                                <Form.Control value={user.phone} onChange={handleChange} name='phone' className='rounded-3' type='text' />
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group controlId='role'>
                                <Form.Label className='mb-1'>Role</Form.Label>
                                <Form.Select value={user.role_id} onChange={handleChange} name='role_id' className='rounded-3'>
                                    {roles?.map(({id, name}) => {
                                        return <option key={id} value={id}>{name}</option>
                                    })}
                                </Form.Select>
                            </Form.Group>
                        </Col>
                        {id ? null : <AdditionalCol handleChange={handleChange} />}
                    </Row>
                </Form>
                <div className='mt-4 d-flex justify-content-end align-items-center gap-2'>
                    <Button onClick={handleSubmit} className='rounded-3' variant='primary'>{id ? 'Update' : 'Add'}</Button>
                    <Button onClick={handleClear} className='rounded-3' variant='secondary'>Reset</Button>
                </div>
            </div>
        </React.Fragment>
    )
}

export default AddUser

const AdditionalCol = ({handleChange}) => {
    return (
        <React.Fragment>
            <Col>
                <Form.Group controlId='salary'>
                    <Form.Label className='mb-1'>Salary</Form.Label>
                    <Form.Control onChange={handleChange} name='salary' className='rounded-3' type='number' />
                </Form.Group>
            </Col>
            <Col>
                <Form.Group controlId='hire_date'>
                    <Form.Label className='mb-1'>Hired Date</Form.Label>
                    <Form.Control onChange={handleChange} name='hire_date' className='rounded-3' type='datetime-local' />
                </Form.Group>
            </Col>
            <Col>
                <Form.Group controlId='password'>
                    <Form.Label className='mb-1'>Password</Form.Label>
                    <Form.Control onChange={handleChange} name='password' className='rounded-3' type='password' />
                </Form.Group>
            </Col>
            <Col>
                <Form.Group controlId='confirm_password'>
                    <Form.Label className='mb-1'>Confirm Password</Form.Label>
                    <Form.Control onChange={handleChange} name='confirm_password' className='rounded-3' type='password' />
                </Form.Group>
            </Col>
        </React.Fragment>
    )
}
