import React, {createContext, useEffect, useState} from 'react'
import { useSelector } from 'react-redux'
import { useUser } from '../core/hook'
import { columns } from './table/column'
import { Button } from 'react-bootstrap'
import TableContainer from '../../../layout/table/components/TableContainer'
import { MdDelete } from 'react-icons/md'
import { Link } from 'react-router-dom'
import { useAuth } from '../../auth/core/hook'


export const UserContext = createContext();

const User = () => {
    const { list,params } = useSelector(state => state.user);
    const { getUsers, deleteUser,onSetParams } = useUser();
    const { checkScope } = useAuth();
    const [id, setId] = useState('');
    const [select,setSelect] =useState("");
    const handleSelect = (e)=>{
        setSelect(e.target.value);
    }

    
    useEffect(() => {
        getUsers();
    }, [params]);


    const [selectedRows, setSelectedRows] = React.useState([]);
    
    const handleRowSelected = React.useCallback(state => {
		setSelectedRows(state.selectedRows);
	}, []);

    const contextActions = React.useMemo(() => {
        const handleDelete = () => {
            if (window.confirm('Are you sure to delete?')) {
                deleteUser(selectedRows.map(row => row.id));
            }
        }

		return (
            <Button onClick={handleDelete} className='rounded-3' variant='danger' size='sm'><MdDelete className='me-1 mb-1' /> Delete</Button>
		);
	}, [selectedRows, list]);

    const onSort = (column, sortDirection) => {
       onSetParams({
           sort:column.sortField,
           order:sortDirection
       });
    }
    return (
        <React.Fragment>
            <UserContext.Provider value={setId}>
                {checkScope('create-user') ? (
                    <div className='mb-4'>
                        <Link to='/user/add'>
                            <Button variant='primary' className='rounded-3'>Add User</Button>
                        </Link>
                    </div>
                ) : null}
                <div className='bg-white mb-4 rounded-4 overflow-hidden'>
                    <div>

                    </div>
                    <TableContainer onSort={onSort}  title='User List' columns={columns} data={list} handleRowSelected={handleRowSelected} contextActions={checkScope('delete-user') ? contextActions : undefined} pagination={true} />
                </div>
            </UserContext.Provider>
        </React.Fragment>
    )
}

export default User