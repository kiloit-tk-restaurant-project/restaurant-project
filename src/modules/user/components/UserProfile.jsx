import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";
import {Card, Row, Col, Image} from 'react-bootstrap';
import '../../../assets/css/components/UserCard.css';
import {useUser} from "../core/hook";


const UserProfile = () => {

    const noneAvatar="https://media.istockphoto.com/id/1223671392/vector/default-profile-picture-avatar-photo-placeholder-vector-illustration.jpg?s=612x612&w=0&k=20&c=s0aTdmT5aU6b8ot7VKm11DeID6NctRCpB755rA1BIP0=";
    const { id } = useParams();
    const {user} = useSelector(state => state.user);
    const { getUserById} = useUser();
    console.log(user);
    const {name,email,avatar,gender,phone,salary,hireDate,status}=user;
    useEffect(() => {
      getUserById(id);
    }, [id]);

    return (
        <div className="App">

            <div className="align-self-center ">
                <h5 className=''>User Profile</h5>
                <Row className="justify-content-center ">
                    <Col xl={6} md={12}>
                        <Card className="user-card-full">
                            <Row className="no-gutters">
                                <Col sm={4} className="bg-c-lite-green user-profile">
                                    <Card.Body className="text-center text-white">
                                        <div className="mb-3">
                                            <Image className='user-img' src={avatar||noneAvatar}
                                                   roundedCircle alt="User-Profile-Image"/>
                                        </div>
                                        <h6 className="f-w-600 mt-3">{name}</h6>
                                        <p>{user.username}</p>
                                        <i className="mdi mdi-square-edit-outline feather icon-edit m-t-10 f-16"></i>
                                    </Card.Body>
                                </Col>
                                <Col sm={8}>
                                    <Card.Body>
                                        <h6 className="m-b-20 p-b-5 b-b-default f-w-600">Information</h6>
                                        <Row>
                                            <Col sm={6}>
                                                <p className="m-b-10 f-w-600">Email</p>
                                                <h6 className="text-muted f-w-400">{email}</h6>
                                            </Col>
                                            <Col sm={6}>
                                                <p className="m-b-10 f-w-600">Phone</p>
                                                <h6 className="text-muted f-w-400">{phone}</h6>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={6}>
                                                <p className="m-b-10 f-w-600">Gender</p>
                                                <h6 className="text-muted f-w-400">{gender}</h6>
                                            </Col>
                                            <Col sm={6}>
                                                <p className="m-b-10 f-w-600">Salary</p>
                                                <h6 className="text-muted f-w-400">{salary}</h6>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col sm={6}>
                                                <p className="m-b-10 f-w-600">Data Of Birth</p>
                                                <h6 className="text-muted f-w-400">{hireDate}</h6>
                                            </Col>
                                        </Row>
                                        <h6 className="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">Permission</h6>
                                        <Row>
                                            <Col sm={6}>
                                                <p className="m-b-10 f-w-600">Create By</p>
                                                <h6 className="text-muted f-w-400">{name}</h6>
                                            </Col>
                                            <Col sm={6}>
                                                <p className="m-b-10 f-w-600">Update By</p>
                                                <h6 className="text-muted f-w-400">{name}</h6>
                                            </Col>
                                        </Row>
                                        <h6 className="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">Action</h6>
                                        <Row>
                                            <Col sm={6}>
                                                <p className="m-b-10 f-w-600">Status</p>
                                                <h6 className="text-muted f-w-400">{status ? 'Ture' : 'false'}</h6>
                                            </Col>
                                        </Row>
                                        <ul className="social-link list-unstyled m-t-40 m-b-10">
                                            <li><a href="#!" data-toggle="tooltip" title="facebook"><i
                                                className="mdi mdi-facebook feather icon-facebook facebook"></i></a>
                                            </li>
                                            <li><a href="#!" data-toggle="tooltip" title="twitter"><i
                                                className="mdi mdi-twitter feather icon-twitter twitter"></i></a></li>
                                            <li><a href="#!" data-toggle="tooltip" title="instagram"><i
                                                className="mdi mdi-instagram feather icon-instagram instagram"></i></a>
                                            </li>
                                        </ul>
                                    </Card.Body>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </div>

        </div>
    );
};

export default UserProfile;