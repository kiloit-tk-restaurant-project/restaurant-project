import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import User from './components/User'
import AddUser from './components/AddUser'
import UserProfile from "./components/UserProfile";

const UserPage = () => {
    return (
        <Routes>
            <Route index path='list' element={<User />} />
            <Route path='add' element={<AddUser />} />
            <Route path='update/:id' element={<AddUser />} />
            <Route path='*' element={<Navigate to='list' />} />
            <Route path='profile/:id' element={<UserProfile/>}/>
        </Routes>
    )
}

export default UserPage