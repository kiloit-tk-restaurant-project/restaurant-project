import axios from 'axios'


const userGetReqFiltering = (params={}) => {
    return axios.get(`/api/user`,{
        params:params
    });
}


const userById=(id)=>{
    return axios.get(`/api/user/${id}`)
}
const userPostReq = (payload) => {
    return axios.post('/api/user', payload);
}
const userDeleteReq = (id) => {
    return axios.delete(`api/user/${id}`);
}
const reqUpdate=(id, payload={})=>{
    return axios.put(`/api/user/${id}`,payload);
}
const reqUploadProfile = (id, file) => {
    return axios.post(`/api/user/${id}/profile-avatar`, file, {
        headers: {
            'Content-Type': 'multipart/form-data',
        }
    });
}
const reqUpdateStatus = (id) => {
    return axios.patch(`/api/user/${id}/status`);
}

export { userGetReqFiltering, userPostReq, userDeleteReq, userById, reqUpdate, reqUploadProfile, reqUpdateStatus };