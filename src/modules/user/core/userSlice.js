import { createSlice } from '@reduxjs/toolkit'
// import state from "sweetalert/typings/modules/state";
// import {actions} from "react-table/src";

const initUser={
    name: '',
    username: '',
    gender: '',
    email: '',
    password: '',
    confirm_password: '',
    phone: '',
    salary: '',
    hire_date: '',
    role_id: '',
    avatar: '',
}
export const userSlice = createSlice({
    name: 'user',
    initialState: {
        list: [],
        user:{...initUser} ,
        addList:[],
        viewUser: {},
        loading:false,
        params:{
            sort:"",
            order:""
        },
        profileUrl: null,
    },
    reducers: {
        fetchUsers: (state, action) => {
            state.list = action.payload;
        },
        setUser: (state, action) => {
            state.user = {...state.user,...action.payload}
        },
        clearUserFields: (state) => {
            state.user = {
               ...initUser
            }
        },
        setLoading:(state,action)=>{
            state.loading=action.payload;
        },
        storeParams: (state, action) => {
            state.params = {...state.params, ...action.payload}
          },
        // useState
        setProfileUrl: (state, action) => {
            state.profileUrl = action.payload;
        }
        // useState
    }
});

export const { fetchUsers, setUser, clearUserFields , storeParams,setViewById ,setLoading, setProfileUrl} = userSlice.actions;
export default userSlice.reducer;