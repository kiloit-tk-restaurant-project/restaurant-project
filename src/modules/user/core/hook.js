import {useDispatch, useSelector} from 'react-redux'
import {
    userGetReqFiltering,
    userPostReq,
    userDeleteReq,
    reqUpdateStatus,
    userById,
    reqUploadProfile,
    reqUpdate
} from './request'
import {fetchUsers, setProfileUrl, setUser, storeParams} from './userSlice'
import {setLoading} from "./userSlice"
import { useNavigate } from 'react-router-dom'

const useUser = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { params } = useSelector(state => state.user);


    const getUsers = () => {
        dispatch(setLoading(true));
        userGetReqFiltering(params).then((res)=>{
            dispatch(fetchUsers(res.data.data));
        });
        dispatch(setLoading(false));
    }

    const getUserById = (id) => {
        return userById(id).then((res)=>{
            dispatch(setUser(res.data.data))
        }).catch(err => console.log(err));
    }
    const putUser=(id,payload)=>{
        return reqUpdate(id,payload).then(()=>{
            alert('Upload Successfully!');
        }).catch((err)=>console.log(err))
    }
  const postUser = (payload) => {
        return userPostReq(payload).then(() => {
            alert('Added successfully!');
            navigate('/user');
        }).catch(err => {
            console.log(err);
        })
    }

    const uploadProfile=(id, file)=>{
        return reqUploadProfile(id, file).then(()=>{
        }).catch(err=>{
            console.log(err);
        })
    }

    const deleteUser = (id) => {
        return userDeleteReq(id).then(() => {
            alert('Deleted successfully!');
            navigate('/user');
        }).catch(err => {
            console.log(err);
        })
    }

    const updateStatus = (id) => {
        return reqUpdateStatus(id).then(res => console.log(res)).catch(err => console.log(err));
    }
    const onSetParams = (params) =>dispatch(storeParams(params))

    const setProfile= (payload)=>dispatch(setProfileUrl(payload))

    return { getUsers, getUserById, postUser, deleteUser ,onSetParams, updateStatus, uploadProfile ,putUser,setProfile}
}

export { useUser };