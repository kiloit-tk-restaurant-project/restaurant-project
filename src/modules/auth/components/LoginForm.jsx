import { useState, useEffect, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setAuthFields } from '../core/authSlice'
import { useAuth } from '../core/hook'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import { LuLogIn } from 'react-icons/lu'

const LoginForm = () => {
    const [hidden, setHidden] = useState(true);
    const focusInput = useRef(null);
    const { auth } = useSelector((state) => state.auth);
    const dispatch = useDispatch();
    const { onLogin } = useAuth();

    useEffect(() => {
        focusInput.current.focus();
    }, []);

    const toggleHidden = () => setHidden(!hidden);
    const handleChange = (e) => {
        dispatch(setAuthFields({ ...auth, [e.target.name]: e.target.value }));
    };
    const handleLogin = (e) => {
        e.preventDefault();
        onLogin(auth);
    };

    return (
        <div className='bg-white vh-100 d-flex justify-content-center align-items-center'>
            <Container>
                <Row
                    className='g-3 g-md-0 overflow-hidden'
                    xs={1}
                    sm={1}
                    md={2}
                >
                    <Col>
                        <div className='w-100 overflow-hidden'>
                            <img 
                            className='w-100'
                            src='https://static.vecteezy.com/system/resources/previews/005/879/539/non_2x/cloud-computing-modern-flat-concept-for-web-banner-design-man-enters-password-and-login-to-access-cloud-storage-for-uploading-and-processing-files-illustration-with-isolated-people-scene-free-vector.jpg'
                            alt=''
                            />
                        </div>
                    </Col>
                    <Col>
                        <Form className='login-form bg-white px-4 py-5'>
                            <h2 className='text-center mb-5'>WELCOME BACK</h2>
                            <Form.Group controlId='usernameField' className='mb-3'>
                                <Form.Label>Username</Form.Label>
                                <Form.Control
                                    name='username'
                                    value={auth.username}
                                    onChange={handleChange}
                                    ref={focusInput}
                                    className='rounded-3'
                                    type='text'
                                />
                            </Form.Group>
                            <Form.Group controlId='passwordField' className='mb-3'>
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    name='password'
                                    value={auth.password}
                                    onChange={handleChange}
                                    className='rounded-3'
                                    type={!hidden ? 'text' : 'password'}
                                />
                            </Form.Group>
                            <Form.Group
                                controlId='hiddenShownPassword'
                                className='mb-4 d-flex align-items-center gap-3'
                            >
                                <Form.Check onChange={toggleHidden} />
                                <Form.Label className='mb-0'>Show password?</Form.Label>
                            </Form.Group>
                            <Button className='rounded-3' onClick={handleLogin} variant='primary'>
                                <LuLogIn className='me-2' /> Login
                            </Button>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </div>   
    )
}

export default LoginForm