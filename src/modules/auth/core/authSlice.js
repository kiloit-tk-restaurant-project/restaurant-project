import { createSlice } from '@reduxjs/toolkit'
import { jwtDecode } from 'jwt-decode'

export const authSlice = createSlice({
    name: 'auth',
    initialState: {
        auth: {
            username: '',
            password: '',
        },
        token: localStorage.getItem('token'),
        scopeData: localStorage.getItem('scopeData'),
    },
    reducers: {
        setAuthFields: (state, action) => {
            const { username, password } = action.payload;
            state.auth = { username, password };
        },
        setToken: (state, action) => {
            state.token = action.payload;
            localStorage.setItem('token', state.token);
            state.scopeData = jwtDecode(state.token);
            localStorage.setItem('scopeData', JSON.stringify(state.scopeData));
        },
    },
})

export const { setAuthFields, setToken, setScopeData } = authSlice.actions
export default authSlice.reducer