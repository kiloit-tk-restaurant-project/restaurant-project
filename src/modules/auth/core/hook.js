import { useDispatch } from 'react-redux'
import { loginReq } from './request'
import { setToken } from './authSlice'

const useAuth = () => {
    const dispatch = useDispatch();

    const onLogin = (payload) => {
        loginReq(payload).then((res) => {
            dispatch(setToken(res.data.data.token));
            window.location.href = '/';
        }).catch((err) => alert(err));
    }

    const checkScope = (scope) => {
        if (JSON.parse(localStorage.getItem('scopeData')).scope.includes(scope)) {
            return true;
        } else {
            return false;
        }
    }
    return { onLogin, checkScope }
}

export { useAuth }