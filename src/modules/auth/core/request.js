import axios from 'axios'

const loginReq = (payload) => {
    return axios.post('/api/auth/login', payload, {
        headers: { Authorization: 'Basic ZzM6MTIz' },
    });
};

export { loginReq }