import axios from 'axios'

const tableGetReq = () => {
    return axios.get('/api/tables');
}
const tablePostReq = (payload) => {
    return axios.post('/api/tables', payload);
}

const tablePutReq = (id, payload) => {
    return axios.put(`/api/tables/${id}`, payload);
};

const tableDeleteReq = (id) => {
    return axios.delete(`/api/tables/${id}`)
}

export { tableGetReq, tablePostReq , tableDeleteReq, tablePutReq };