import { createSlice } from '@reduxjs/toolkit';


export const tableSlice = createSlice({
    name: 'tables',
    initialState: {
        list: [],
        table: {
            id: '',
            name: '',
            status: 'Available',
            seat_Capacity: ''
        },
        modal: false,
    },
    reducers: {
        fetchTables: (state, action) => {
            state.list = action.payload;
        },
        changeTableFields: (state, action) => {
            const { id, name, seat_Capacity, status } = action.payload;
            state.table = {...state.table, id, name, seat_Capacity, status};
        },
        clearTableFields: (state) => {
            state.table = { id: '', name: '', status: 'Available', seat_Capacity: ''};
        },
        setModal: (state, action) => {
            state.modal = action.payload;
        }
    }
})

export const { fetchTables, changeTableFields, clearTableFields, setModal } = tableSlice.actions;
export default tableSlice.reducer;