import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { tableGetReq, tablePostReq, tablePutReq, tableDeleteReq } from './request'
import { fetchTables } from './tableSlice'

const useTable = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    
    const getTables = () => {
        return tableGetReq().then(res => {
            dispatch(fetchTables(res.data.data));
        }).catch(err => {
            console.log(err);
        })
    }
    const postTable = (payload) => {
        return tablePostReq(payload).then(() => {
            alert('Added successfully!');
            navigate('/table');
        }).catch(err => {
            console.log(err);
        })
    }
    const putTable = (id, payload) => {
        return tablePutReq(id, payload).then(() => {
            alert('Updated successfully!');
            navigate('/table');
        }).catch(err => {
            console.log(err);
        })
    }
    const deleteTable = (id) => {
        return tableDeleteReq(id).then(() => {
            alert('Deleted successfully!');
            navigate('/table');
        }).catch(err => {
            console.log(err);
        })
    }

    return { getTables, postTable, putTable, deleteTable }
}

export { useTable };