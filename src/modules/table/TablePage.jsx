import { Routes, Route, Navigate } from 'react-router-dom'
import Tables from './components/Table'

const TablePage = () => {
    return (
        <Routes>
            <Route index path='list' element={<Tables/>} />
            <Route path='*' element={<Navigate to='list'/>}/>
        </Routes>
    )
}

export default TablePage