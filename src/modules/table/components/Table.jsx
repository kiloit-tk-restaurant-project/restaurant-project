import React, { createContext, useEffect, useState } from 'react'
import { Button, Col, Form, Modal, Row } from 'react-bootstrap'
import { useTable } from '../core/hook'
import { useDispatch, useSelector } from 'react-redux'
import { changeTableFields, clearTableFields, setModal } from '../core/tableSlice'
import TableContainer from '../../../layout/table/components/TableContainer'
import { columns } from './table/column'
import { MdDelete } from 'react-icons/md'

export const TableContext = createContext();

const Roles = () => {
    const { getTables, postTable, putTable, deleteTable } = useTable();
    const {list, table, modal} = useSelector(state => state.table);
    const [addStatus, setAddStatus] = useState(true);
    const dispatch = useDispatch();
    
    const showModal = () => dispatch(setModal(true));
    const hideModal = () => dispatch(setModal(false));

    const handleChange = e => dispatch(changeTableFields({...table, [e.target.name]: e.target.value}));
    
    const handleClear = () => dispatch(clearTableFields());

    const showModalAdd = () => {
        handleClear();
        setAddStatus(true);
        showModal();
    }

    const handleSumit = e => {
        e.preventDefault();
        const {id, name, status, seat_Capacity} = table;
        addStatus ? postTable({name, status, seat_Capacity}) : putTable(id, {name, status, seat_Capacity});
        hideModal();
    }

    useEffect(() => {
        getTables();
    }, []);

    const [selectedRows, setSelectedRows] = React.useState([]);
    
    const handleRowSelected = React.useCallback(state => {
		setSelectedRows(state.selectedRows);
	}, []);

    const contextActions = React.useMemo(() => {
        const handleDelete = () => {
            if (window.confirm('Are you sure to delete?')) {
                deleteTable(selectedRows.map(row => row.id));
            }
        }

		return (
            <Button onClick={handleDelete} className='rounded-3' variant='danger' size='sm'><MdDelete className='me-1 mb-1' /> Delete</Button>
		);
	}, [selectedRows, list]);

    return (
        <TableContext.Provider value={{list, table, setAddStatus}}>
            <React.Fragment>
                <div className='mb-4'>
                    <Button onClick={showModalAdd} variant='primary' className='rounded-3'>Add Table</Button>
                </div>
                <div className='bg-white mb-4 rounded-4 overflow-hidden'>
                    <TableContainer title='Table List' columns={columns} data={list} handleRowSelected={handleRowSelected} contextActions={contextActions} pagination={true} />
                </div>
                <Modal show={modal} onHide={hideModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>{addStatus ? 'New' : 'Udpate'} Table</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group className='mb-3' controlId='name'>
                                <Form.Label className='mb-1'>Name</Form.Label>
                                <Form.Control value={table.name} onChange={handleChange} name='name' className='rounded-3' type='text' />
                            </Form.Group>
                            <Row>
                                <Col xs={5}>
                                    <Form.Group controlId='status'>
                                        <Form.Label className='mb-1'>Status</Form.Label>
                                        <Form.Select onChange={handleChange} name='status' defaultValue={table.status} className='rounded-3'>
                                            <option value='Available'>Available</option>
                                            <option value='Booked'>Booked</option>
                                        </Form.Select>
                                    </Form.Group>
                                </Col>
                                <Col xs={7}>
                                    <Form.Group controlId='seat_Capacity'>
                                        <Form.Label className='mb-1'>Seat</Form.Label>
                                        <Form.Control value={table.seat_Capacity} onChange={handleChange} name='seat_Capacity' className='rounded-3' type='number' />
                                    </Form.Group>
                                </Col>
                            </Row>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={handleSumit} variant='primary' className='rounded-3'>{addStatus ? 'Add' : 'Update'}</Button>
                        <Button onClick={handleClear} variant='secondary' className='rounded-3'>Reset</Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        </TableContext.Provider>
    )
}

export default Roles