import ActionCell from "./ActionCell";

const columns = [
    {
        name: 'Name',
        selector: row => row.name,
    },
    {
        name: 'Status',
        selector: row => row.status,
    },
    {
        name: 'Seat',
        selector: row => row.seat_Capacity,
    },
    {
        name: 'Action',
        selector: row => row.action,
        cell: row => <ActionCell id={row.id} />

    },
];

export {columns};