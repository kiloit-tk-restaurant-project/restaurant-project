import React from 'react';
import FoodImage from "../../../food/components/table/FoodImage";

const ColumnProfile = [
    {
        name:'Image',
        selector:row=>row.avatar,
        cell:row=><FoodImage src={row.avatar}/>
    },
    {
        name: 'Name',
        selector:row => row.name
    },
    {
        userName: 'User Name',
        selector:row => row.userName
    },
    {
        gender:'Gender',
        selector:row => row.gender
    },
    {
        email:'Email',
        selector:row => row.email
    },
    {
        password:'Password',
        selector:row => row.password
    },
    {
        salary:'Salary',
        selector:row => row.salary
    },
    {
        dateOfBirth:'DatOfBirth',
        selector:row => row.dateOfBirth,
    }
]


export{ColumnProfile};