import React, { createContext, useEffect, useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { useRole } from '../core/hook'
import { useDispatch, useSelector } from 'react-redux'
import { changeRoleFields, clearRoleFields, setModal, setPermissions } from '../core/roleSlice'
import TableContainer from '../../../layout/table/components/TableContainer'
import { columns, permissionColumns } from './table/column.jsx'
import { MdDelete } from 'react-icons/md'
import { useAuth } from '../../auth/core/hook'

export const RoleContext = createContext();

const Roles = () => {
    const { getRoles, postRole, putRole, deleteRole, putPermission } = useRole();
    const {list, role, modal, permissions} = useSelector(state => state.role);
    const [addStatus, setAddStatus] = useState(true);
    const [permissionModal, setPermissionModal] = useState(false);
    const dispatch = useDispatch();
    const { checkScope } = useAuth();
    
    const showModal = () => dispatch(setModal(true));
    const hideModal = () => {
        dispatch(setModal(false));
        setPermissionModal(false);
    }

    const handleChange = e => dispatch(changeRoleFields({...role, [e.target.name]: e.target.value}));
    
    const handleClear = () => dispatch(clearRoleFields());

    const showModalAdd = () => {
        handleClear();
        setAddStatus(true);
        showModal();
    }

    const handleSumit = e => {
        e.preventDefault();
        const {id, name, code} = role;
        addStatus ? postRole({name, code}) : putRole(id, {name, code});
        hideModal();
    }

    useEffect(() => {
        getRoles();
    }, []);

    const [selectedRows, setSelectedRows] = React.useState([]);
    
    const handleRowSelected = React.useCallback(state => {
		setSelectedRows(state.selectedRows);
	}, []);
    
    
    const contextActions = React.useMemo(() => {
        const handleDelete = () => {
            if (window.confirm('Are you sure to delete?')) {
                deleteRole(selectedRows.map(row => row.id));
            }
        }
        
		return (
            <Button onClick={handleDelete} className='rounded-3' variant='danger' size='sm'><MdDelete className='me-1 mb-1' /> Delete</Button>
		);
	}, [selectedRows, list]);

    const preSelectedRow = row => !!(row.status);

    const handlePermissionsSelected = (rows) => {
        const selectedPermissions = rows.selectedRows;
        if (selectedPermissions.length > 0) {
            dispatch(setPermissions(permissions.map(permission => {
                const isSelected = selectedPermissions.some(selectedPermission => selectedPermission === permission);
                
                if (isSelected) {
                    return {...permission, status: 1};
                } else {
                    return permission;
                }
            })));
        }
    };

    const updatePermissions = () => {
        const payload = {
            role_id: role.id,
            permissions: permissions.map(({id, status}) => {
                return {
                    permission_id: id,
                    status: !!(status),
                }
            })
        }
        putPermission(payload);
        hideModal();
    }

    return (
        <RoleContext.Provider value={{list, role, setAddStatus, setPermissionModal}}>
            <React.Fragment>
                {checkScope('create-role') ? (
                    <div className='mb-4'>
                        <Button onClick={showModalAdd} variant='primary' className='rounded-3'>Add Role</Button>
                    </div>
                ) : null}
                <div className='bg-white mb-4 rounded-4 overflow-hidden'>
                    <TableContainer title='Role List' columns={columns} data={list} handleRowSelected={handleRowSelected} contextActions={checkScope('delete-role') ? contextActions : undefined} pagination={true} />
                </div>
                <Modal show={modal} onHide={hideModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>{addStatus ? 'New' : 'Udpate'} Role</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group className='mb-3' controlId='name'>
                                <Form.Label className='mb-1'>Name</Form.Label>
                                <Form.Control value={role.name} onChange={handleChange} name='name' className='rounded-3' type='text' />
                            </Form.Group>
                            <Form.Group controlId='code'>
                                <Form.Label className='mb-1'>Code</Form.Label>
                                <Form.Control value={role.code} onChange={handleChange} name='code' className='rounded-3' text='text' />
                            </Form.Group>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={handleSumit} variant='primary' className='rounded-3'>{addStatus ? 'Add' : 'Update'}</Button>
                        <Button onClick={handleClear} variant='secondary' className='rounded-3'>Reset</Button>
                    </Modal.Footer>
                </Modal>
                <Modal size='lg' show={permissionModal} onHide={hideModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Permission Allowance</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <TableContainer handleRowSelected={handlePermissionsSelected} selecttableRowSelected={preSelectedRow} title='Permission List' columns={permissionColumns} data={permissions} pagination={false} />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={updatePermissions} variant='primary' className='rounded-3'>Save</Button>
                        <Button onClick={hideModal} variant='secondary' className='rounded-3'>Cancel</Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        </RoleContext.Provider>
    )
}

export default Roles