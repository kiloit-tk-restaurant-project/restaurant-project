import React, { useContext } from 'react'
import { IoMdMore } from 'react-icons/io'
import { FaEdit } from 'react-icons/fa'
import { IoSettingsSharp } from 'react-icons/io5'
import { useDispatch, useSelector } from 'react-redux'
import { changeRoleFields, setModal, setPermissions } from '../../core/roleSlice'
import { RoleContext } from '../Roles'
import { useRole } from '../../core/hook'

const ActionCell = ({id}) => {
    const { list, role, setAddStatus, setPermissionModal } = useContext(RoleContext);
    const dispatch = useDispatch();
    const { getRoleById } = useRole();

    const onUpdate = () => {
        setAddStatus(false);
        dispatch(setModal(true));
        const {name, code} = list.find(role => role.id === id);
        dispatch(changeRoleFields({...role, id, name, code}));
    };

    const onPermission = () => {
        getRoleById(id);
        dispatch(changeRoleFields({...role, id}));
        setPermissionModal(true);
    }

    return (
        <div className='dropdown'>
            <span className='dropdown-toggler d-flex justify-content-center align-items-center' data-bs-toggle='dropdown'>
                <IoMdMore className='fs-5' />
            </span>
            <ul className='dropdown-menu'>
                <li>
                    <a onClick={onPermission} className='dropdown-item' role='button'><IoSettingsSharp className='me-2 mb-1' />Permission</a>
                </li>
                <li>
                    <a onClick={onUpdate} className='dropdown-item' role='button'><FaEdit className='me-2 mb-1' />Update</a>
                </li>
            </ul>
        </div>
    )
}

export default ActionCell
