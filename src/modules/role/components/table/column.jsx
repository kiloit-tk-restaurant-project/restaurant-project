import ActionCell from './ActionCell'

const columns = [
    {
        name: 'Name',
        selector: row => row.name,
    },
    {
        name: 'Code',
        selector: row => row.code,
    },
];

if (JSON.parse(localStorage.getItem('scopeData'))?.scope.includes('edit-role')) {
    columns.push(
        {
            name: 'Action',
            cell: row => <ActionCell id={row.id} />
        }
    );
}

const permissionColumns = [
    {
        name: 'Permission',
        selector: row => row.name,
        right: true,
    },
];

export { columns, permissionColumns };