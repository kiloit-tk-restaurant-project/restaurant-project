import axios from 'axios'

const roleGetReq = () => {
    return axios.get('/api/roles');
}
const roleGetReqById = (id) => {
    return axios.get(`/api/roles/${id}`);
}
const rolePostReq = (payload) => {
    return axios.post('/api/roles', payload);
}
const rolePutReq = (id, payload) => {
    return axios.put(`/api/roles/${id}`, payload);
}
const roleDeleteReq = (id) => {
    return axios.delete(`/api/roles/${id}`);
}
const permissionPutReq = (payload) => {
    return axios.put('/api/roles/permission', payload);
}

export { roleGetReq, roleGetReqById, rolePostReq, rolePutReq, roleDeleteReq, permissionPutReq };