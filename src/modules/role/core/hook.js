import { useDispatch } from 'react-redux'
import { roleGetReq, roleGetReqById, rolePostReq, rolePutReq, roleDeleteReq, permissionPutReq } from './request'
import { fetchRoles, setPermissions } from './roleSlice'
import { useNavigate } from 'react-router-dom';

const useRole = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const getRoles = () => {
        return roleGetReq().then(res => {
            dispatch(fetchRoles(res.data.data));
        }).catch(err => console.log(err));
    }
    const getRoleById = (id) => {
        return roleGetReqById(id).then(res => {
            dispatch(setPermissions(res.data.data.permissions));
        }).catch(err => console.log(err));
    }
    const postRole = (payload) => {
        return rolePostReq(payload).then(() => {
            alert('Added successfully!');
            navigate('/role');
        }).catch(err => {
            console.log(err);
        })
    }
    const putRole = (id, payload) => {
        return rolePutReq(id, payload).then(() => {
            alert('Updated successfully!');
            navigate('/role');
        }).catch(err => {
            console.log(err);
        })
    }
    const deleteRole = (id) => {
        return roleDeleteReq(id).then(() => {
            alert('Deleted successfully!');
            navigate('/role');
        }).catch(err => {
            console.log(err);
        })
    }
    const putPermission = (payload) => {
        return permissionPutReq(payload).then(() => {
            alert('Updated successfully!');
        }).catch(err => {
            console.log(err);
        })
    }

    return { getRoles, getRoleById, postRole, putRole, deleteRole, putPermission }
}

export { useRole };