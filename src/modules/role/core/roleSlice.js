import { createSlice } from '@reduxjs/toolkit'

export const roleSlice = createSlice({
    name: 'role',
    initialState: {
        list: [],
        role: {
            id: '',
            name: '',
            code: '',
        },
        modal: false,
        permissions: [],
    },
    reducers: {
        fetchRoles: (state, action) => {
            state.list = action.payload;
        },
        changeRoleFields: (state, action) => {
            const { id, name, code } = action.payload;
            state.role = { id, name, code };
        },
        clearRoleFields: (state) => {
            state.role = { id: '', name: '', code: '' };
        },
        setModal: (state, action) => {
            state.modal = action.payload;
        },
        setPermissions: (state, action) => {
            state.permissions = action.payload;
        }
    }
});

export const { fetchRoles, changeRoleFields, clearRoleFields, setModal, setPermissions } = roleSlice.actions;
export default roleSlice.reducer;