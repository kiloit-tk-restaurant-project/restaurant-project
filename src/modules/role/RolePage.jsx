import React from 'react'
import { Routes, Route, Navigate } from 'react-router-dom'
import Roles from './components/Roles'

const RolePage = () => {
    return (
        <Routes>
            <Route index path='list' element={<Roles />} />
            <Route path='*' element={<Navigate to='list' />} />
        </Routes>
    )
}

export default RolePage