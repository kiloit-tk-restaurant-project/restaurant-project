import React from 'react'
import { Routes, Route, Navigate } from 'react-router-dom'
import Food from './components/Food'

const FoodPage = () => {
    return (
        <Routes>
            <Route index path='list' element={<Food />} />
            <Route path='*' element={<Navigate to='list' />} />
        </Routes>
    )
}

export default FoodPage