import React, { useContext } from 'react'
import { IoMdMore } from 'react-icons/io'
import { FaEdit } from 'react-icons/fa'
import { FaEye } from 'react-icons/fa'
import { FoodContext } from '../Food'
import { useDispatch } from 'react-redux'
import { changeFoodFields, setModal } from '../../core/foodSlice'

const ActionCell = ({id}) => {
    const {list, food, setAddStatus} = useContext(FoodContext);
    const dispatch = useDispatch();

    const onUpdate = () => {
        setAddStatus(false);
        dispatch(setModal(true));
        const {name, code, foodImage, price, discount, description, food_categoryId} = list.find(food => food.id === id);
        dispatch(changeFoodFields({...food, id, name, code, foodImage, price, discount, description, food_categoryId}));
    };

    return (
        <div className='dropdown'>
            <span className='dropdown-toggler d-flex justify-content-center align-items-center' data-bs-toggle='dropdown'>
                <IoMdMore className='fs-5' />
            </span>
            <ul className='dropdown-menu'>
                <li>
                    <a onClick={onUpdate} className='dropdown-item' role='button'><FaEye className='me-2 mb-1' />View details</a>
                </li>
                <li>
                    <a onClick={onUpdate} className='dropdown-item' role='button'><FaEdit className='me-2 mb-1' />Update</a>
                </li>
            </ul>
        </div>
    )
}

export default ActionCell