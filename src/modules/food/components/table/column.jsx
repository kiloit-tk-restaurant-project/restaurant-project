import ActionCell from "./ActionCell";
import FoodImage from "./FoodImage";

const columns = [
    {
        name: 'Image',
        selector: row => row.foodImage,
        cell: row => <FoodImage src={row.foodImage} />
    },
    {
        name: 'Name',
        selector: row => row.name,
    },
    {
        name: 'Code',
        selector: row => row.code,
    },
    {
        name: 'Price',
        selector: row => `$ ${row.price.toFixed(2)}`,
    },
    {
        name: 'Discount',
        selector: row => `% ${(row.discount || 0).toFixed(2)}`,
    },
    {
        name: 'Status',
        selector: row => Number(row.status),
    },
    {
        name: 'Action',
        cell: row => <ActionCell id={row.id} />
    },
];

export { columns };