import React, { createContext, useEffect, useState } from 'react'
import { Button, Col, Form, Modal, Row } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { useFood } from '../core/hook'
import TableContainer from '../../../layout/table/components/TableContainer'
import { columns } from './table/column'
import { MdDelete } from 'react-icons/md'
import { changeFoodFields, clearFoodFields, setModal } from '../core/foodSlice'
import { useCategory } from '../../foodCategory/core/hook'

export const FoodContext = createContext();

const Food = () => {
    const { getFoods, postFood, putFood, deleteFood, putImage } = useFood();
    const {list, food, modal} = useSelector(state => state.food);
    const { getCategories } = useCategory();
    const categories = useSelector(state => state.category.list);
    const [addStatus, setAddStatus] = useState(true);
    const dispatch = useDispatch();
    
    const showModal = () => dispatch(setModal(true));
    const hideModal = () => dispatch(setModal(false));

    const handleChange = e => dispatch(changeFoodFields({...food, [e.target.name]: e.target.value}));
    
    const handleClear = () => dispatch(clearFoodFields());

    const showModalAdd = () => {
        handleClear();
        dispatch(changeFoodFields({...food, food_categoryId: categories[0].id}));
        setAddStatus(true);
        showModal();
    }

    const handleSumit = e => {
        e.preventDefault();
        const {id, name, code, foodImage, price, discount, description, food_categoryId} = food;
        addStatus ? postFood({name, code, price, discount, description, food_categoryId: Number(food_categoryId)}) : putFood(id, {name, code, foodImage, price, discount, description, food_categoryId});
        hideModal();
    }

    const handleUpdateImage = e => {
        putImage(food.id, e.target.files[0]);
    }
    
    useEffect(() => {
        getFoods();
        getCategories();
    }, []);

    const [selectedRows, setSelectedRows] = React.useState([]);
    
    const handleRowSelected = React.useCallback(state => {
		setSelectedRows(state.selectedRows);
	}, []);

    const contextActions = React.useMemo(() => {
        const handleDelete = () => {
            if (window.confirm('Are you sure to delete?')) {
                deleteFood(selectedRows.map(row => row.id));
            }
        }

		return (
            <Button onClick={handleDelete} className='rounded-3' variant='danger' size='sm'><MdDelete className='me-1 mb-1' /> Delete</Button>
		);
	}, [selectedRows, list]);

    return (
        <FoodContext.Provider value={{list, food, setAddStatus}}>
            <React.Fragment>
                <div className='mb-4'>
                    <Button onClick={showModalAdd} variant='primary' className='rounded-3'>Add Food</Button>
                </div>
                <div className='bg-white mb-4 rounded-4 overflow-hidden'>
                    <TableContainer title='Food List' columns={columns} data={list} handleRowSelected={handleRowSelected} contextActions={contextActions} pagination={true} />
                </div>
                <Modal size='lg' show={modal} onHide={hideModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>{addStatus ? 'New' : 'Udpate'} Food</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            {addStatus ? null : (
                                <div className='mb-3'>
                                    <img className='food-img-md d-block mb-2' src={food.foodImage} />
                                    <input onChange={handleUpdateImage} id='update-img' type='file' className='d-none' />
                                    <label htmlFor='update-img' className='btn btn-warning btn-sm rounded-3'>Update food image</label>
                                </div>
                            )}
                            <Row className='g-3' xs={1} md={2}>
                                <Col>
                                    <Form.Group controlId='name'>
                                        <Form.Label className='mb-1'>Name</Form.Label>
                                        <Form.Control value={food.name} onChange={handleChange} name='name' className='rounded-3' type='text' />
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Group controlId='code'>
                                        <Form.Label className='mb-1'>Code</Form.Label>
                                        <Form.Control value={food.code} onChange={handleChange} name='code' className='rounded-3' type='text' />
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Group controlId='price'>
                                        <Form.Label className='mb-1'>Price</Form.Label>
                                        <Form.Control value={food.price} onChange={handleChange} name='price' className='rounded-3' type='number' />
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Form.Group controlId='discount'>
                                        <Form.Label className='mb-1'>Discount</Form.Label>
                                        <Form.Control value={food.discount} onChange={handleChange} name='discount' className='rounded-3' type='number' />
                                    </Form.Group>
                                </Col>
                            </Row>    
                            <Form.Group className='my-3' controlId='food_categoryId'>
                                <Form.Label className='mb-1'>Food Category</Form.Label>
                                <Form.Select defaultValue={food.food_categoryId} onChange={handleChange} name='food_categoryId' className='rounded-3'>
                                    {categories?.map(({id, name}) => {
                                        return <option key={id} value={Number(id)}>{name}</option>
                                    })}
                                    <option value=''></option>
                                </Form.Select>
                            </Form.Group>
                            <Form.Group controlId='description'>
                                <Form.Label className='mb-1'>Description</Form.Label>
                                <Form.Control as='textarea' value={food.description} onChange={handleChange} name='description' className='rounded-3' type='text' />
                            </Form.Group>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={handleSumit} variant='primary' className='rounded-3'>{addStatus ? 'Add' : 'Update'}</Button>
                        <Button onClick={handleClear} variant='secondary' className='rounded-3'>Reset</Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        </FoodContext.Provider>
    )
}

export default Food