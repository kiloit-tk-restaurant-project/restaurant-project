import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { foodGetReq, foodPostReq, foodPutReq, foodDeleteReq, foodImagePutReq } from './request'
import { fetchFoods } from './foodSlice'

const useFood = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    
    const getFoods = () => {
        return foodGetReq().then(res => {
            dispatch(fetchFoods(res.data.data));
        }).catch(err => {
            console.log(err);
        })
    }
    const postFood = (payload) => {
        return foodPostReq(payload).then(() => {
            alert('Added successfully!');
            navigate('/food');
        }).catch(err => {
            console.log(err);
        })
    }
    const putFood = (id, payload) => {
        return foodPutReq(id, payload).then(() => {
            alert('Updated successfully!');
            navigate('/food');
        }).catch(err => {
            console.log(err);
        })
    }
    const deleteFood = (id) => {
        return foodDeleteReq(id).then(() => {
            alert('Deleted successfully!');
            navigate('/food');
        })
    }
    const putImage = (id, payload) => {
        return foodImagePutReq(id, payload).then(res => console.log(res)).catch(err => console.log(err));
    }

    return { getFoods, postFood, putFood, deleteFood, putImage }
}

export { useFood };