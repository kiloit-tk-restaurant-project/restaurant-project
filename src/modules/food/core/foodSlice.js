import { createSlice } from '@reduxjs/toolkit'

export const foodSlice = createSlice({
    name: 'food',
    initialState: {
        list: [],
        food: {
            id: '',
            name: '',
            code: '',
            foodImage: '',
            price: '',
            discount: null,
            description: '',
            food_categoryId: '',
        },
        modal: false,
    },
    reducers: {
        fetchFoods: (state, action) => {
            state.list = action.payload;            
        },
        changeFoodFields: (state, action) => {
            const { id, name, code, foodImage, price, discount, description, food_categoryId } = action.payload;
            state.food = { id, name, code, foodImage, price, discount, description, food_categoryId };
        },
        clearFoodFields: (state) => {
            state.food = { id: '', name: '', code: '', foodImage: '', price: '', discount: null, description: '', food_categoryId: ''};
        },
        setModal: (state, action) => {
            state.modal = action.payload;
        }
    }
});

export const { fetchFoods, changeFoodFields, clearFoodFields, setModal } = foodSlice.actions;
export default foodSlice.reducer;