import axios from 'axios'

const foodGetReq = () => {
    return axios.get('/api/foods');
}
const foodPostReq = (payload) => {
    return axios.post('/api/foods', payload);
}
const foodPutReq = (id, payload) => {
    return axios.put(`/api/foods/${id}`, payload);
}
const foodDeleteReq = (id) => {
    return axios.delete(`/api/foods/${id}`);
}
const foodImagePutReq = (id, payload) => {
    return axios.put(`/api/food/images/${id}`, payload, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });
}

export { foodGetReq, foodPostReq, foodPutReq, foodDeleteReq, foodImagePutReq };