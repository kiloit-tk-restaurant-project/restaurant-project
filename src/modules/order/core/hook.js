import { useDispatch } from 'react-redux'
import { orderGetReq, orderPostReq } from './request'
import { fetchOrderItems, fetchOrders } from './orderSlice'

const useOrder = () => {
    const dispath = useDispatch();

    const getOrders = () => {
        return orderGetReq().then(res => dispath(fetchOrders(res.data.data))).catch(err => console.log(err));
    }
    const postOrder = (payload) => {
        return orderPostReq(payload).then(() => {
            alert('Ordered successfully!');
            dispath(fetchOrderItems([]));
        }).catch(err => console.log(err));
    }

    return { getOrders, postOrder }
}

export { useOrder };