import axios from 'axios'

const orderGetReq = () => {
    return axios.get('/api/orders');
}
const orderPostReq = (payload) => {
    return axios.post('/api/orders', payload);
}

export { orderGetReq, orderPostReq };