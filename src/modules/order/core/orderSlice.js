import { createSlice } from '@reduxjs/toolkit'

export const orderSlice = createSlice({
    name: 'order',
    initialState: {
        list: [],
        orderItems: [],
    },
    reducers: {
        fetchOrderItems: (state, action) => {
            state.orderItems = action.payload;
        },
        fetchOrders: (state, action) => {
            state.list = action.payload;
        }
    }
})

export const { fetchOrderItems, fetchOrders } = orderSlice.actions;
export default orderSlice.reducer;