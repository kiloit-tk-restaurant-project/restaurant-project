import { Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'


import { MdOutlinePayments } from "react-icons/md";
const MoreInfo = () => {
    return (
        //Button in order component (other information)
        <div>
            <div className="dropdown">
                <Button className="btn-more-info btn btn-primary dropdown-toggle"
                    href="/" role="button" data-bs-toggle="dropdown"
                    aria-expanded="false">
                    <MdOutlinePayments className='me-1'/>
                </Button>
                <ul className="dropdown-menu text-decoration-none list-unstyled">
                    <Link to='/order/payments'>
                        <li className='d-flex justify-content-center align-content-center'>Payments</li>
                    </Link>
                </ul>
            </div>
        </div>
    )
}

export default MoreInfo

