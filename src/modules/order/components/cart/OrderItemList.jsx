import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Offcanvas, Row } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { IoIosRemoveCircle } from 'react-icons/io'
import { fetchOrderItems } from '../../core/orderSlice'
import { useTable } from '../../../table/core/hook'
import { useOrder } from '../../core/hook'

const OrderItemList = ({show, onHide}) => {
    const { orderItems } = useSelector(state => state.order);
    const tables = useSelector(state => state.table.list);
    const [table_id, setTable_id] = useState('');
    const dispatch = useDispatch();
    const { getTables } = useTable();
    const { postOrder } = useOrder();

    const handleRemove = id => {
        const afterRemoval = orderItems.filter(item => item.food_id !== id);
        dispatch(fetchOrderItems(afterRemoval));
    }

    const handleTableSelection = e => {
        setTable_id(Number(e.target.value));
    }

    const handleOrder = () => {
        const items = orderItems.map(({food_id, quantity}) => {
            return {food_Id: food_id, quantity: quantity};
        });
        postOrder({
            table_Id: table_id,
            user_Id: JSON.parse(localStorage.getItem('scopeData')).id,
            items,
        });
    }
    
    useEffect(() => {
        getTables();
    }, []);

    return (
        <Offcanvas placement='end' show={show} onHide={onHide}>
            <Offcanvas.Header closeButton>
                <Offcanvas.Title>Your Card</Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body className='position-relative'>
                <ul className='list-unstyled d-flex flex-column gap-3'>
                    {orderItems.map(({food_id, foodImage, name, price, discount, quantity}) => {
                        return (
                            <li key={food_id}>
                                <div className='d-flex gap-4 justify-content-start align-item-center'>
                                    <img className='food-img' src={foodImage || 'https://assets.epicurious.com/photos/5c745a108918ee7ab68daf79/master/pass/Smashburger-recipe-120219.jpg'} alt='' />
                                    <div className='d-flex flex-column justify-content-between w-100'>
                                        <p className='mb-0 d-flex gap-2 justify-content-between align-item-center'>{name}<IoIosRemoveCircle onClick={() => handleRemove(food_id)} className='text-danger fs-6 cursor-pointer' /></p>
                                        <p className='mb-0 d-flex gap-2 justify-content-between align-item-center'>Quantity <span>{quantity}</span></p>
                                        <p className='mb-0 fw-bold d-flex gap-2 justify-content-between align-item-center'>${price.toFixed(2)}<span className='text-danger'>-%${discount ? discount.toFixed(2) : Number(0).toFixed(2)}</span></p>
                                    </div>
                                </div>
                            </li>
                        )
                    })}
                </ul>
                <h6 className='mt-4'>Choose Table</h6>
                <div>
                    {tables.filter(({status}) => status === 'Available').map(({id, name, seat_Capacity}) => {
                        return (
                            <Row key={id} className='g-3 align-items-center' xs={2}>
                                <Col xs={8}>
                                    <Form.Group controlId={`table-${id}`} className='d-flex gap-3'>
                                        <Form.Check value={id} onChange={handleTableSelection} type='radio' name='seat' />
                                        <Form.Label>{name}</Form.Label>
                                    </Form.Group>
                                </Col>
                                <Col xs={4}>
                                    <p className='text-end'>{seat_Capacity} Seat</p>
                                </Col>
                            </Row>        
                        )
                    })}
                </div>
                <Button onClick={handleOrder} variant='primary' className='rounded-3 d-block w-100 float-bottom mt-5'>Order</Button>
            </Offcanvas.Body>
        </Offcanvas>
    )
}

export default OrderItemList