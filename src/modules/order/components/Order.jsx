import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Row } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { useCategory } from '../../foodCategory/core/hook'
import { useFood } from '../../food/core/hook'
import FoodCard from './card/FoodCard'
import { FaListUl } from 'react-icons/fa'
import { Link } from 'react-router-dom'

const Order = () => {
    const categories = useSelector(state => state.category.list);
    const { getCategories } = useCategory();
    const foods = useSelector(state => state.food.list);
    const { getFoods } = useFood();
    const dispatch = useDispatch();

    useEffect(() => {
        getCategories();
        getFoods();
    }, []);

    const handleFilteringCategory = e => {
        if (e.target.value === 'all') {
            getFoods();
        } else {
            getFoods();
            const filteredFoods = foods.filter(food => food.foodCategoryEntity.id == e.target.value);
        }
    }

    return (
        <React.Fragment>
            <div className='mb-4 d-flex justify-content-between align-items-center gap-2'>
                <Form.Select className='w-auto rounded-3'>
                    <option value='all'>All</option>
                    {categories.map(({ id, name }) => {
                        return <option key={id} value={id}>{name}</option>
                    })};
                    {/* {categories.map(({id,name})=>{<option key={id} value={id}>{name}</option>})} */}
                </Form.Select>
                <Link to='/order/list'>
                    <FaListUl className='fs-5 text-dark cursor-pointer' />
                </Link>
            </div>
            <Row className='g-4 mb-4' xs={1} sm={2} lg={3} xl={4}>
                {foods.map(({ id, foodImage, name, description, price, discount }) => {
                    return (
                        <Col key={id}>
                            <FoodCard id={id} foodImage={foodImage} name={name} description={description} price={price} discount={discount} />
                        </Col>
                    )
                })}
            </Row>
        </React.Fragment>
    )
}

export default Order