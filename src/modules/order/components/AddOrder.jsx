import React, { useState } from 'react';
import { useOrder } from '../core/hook'
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { IoArrowBack } from "react-icons/io5";

const AddOrder = () => {
    
    const {addOrder} = useOrder();
    const [add, setAdd] = useState({
        table_Id: "",
        user_Id: "",
        food_Id: "",
        quantity: ""
    });

    const handleChange = (event) => {
        const { name, value } = event.target;
        setAdd(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        addOrder({
            table_Id: parseInt(add.table_Id),
            user_Id: parseInt(add.user_Id),
            items: [
                {
                    food_Id: parseInt(add.food_Id),
                    quantity: parseInt(add.quantity),
                }
            ]
        });
        setAdd({
            table_Id: "",
            user_Id: "",
            food_Id: "",
            quantity: ""
        });
    };

    return (
        <div>
            <div className='mb-4'>
                <Link to='/order'>
                    <Button className='fw-bold'>
                        <IoArrowBack />
                    </Button>
                </Link>
            </div>
            <h2 className='fw-bold'>Add Order</h2>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="table_id" className="form-label">Table ID</label>
                    <input
                        type="text"
                        className="form-control"
                        id="table_Id"
                        name="table_Id"
                        value={add.table_Id}
                        onChange={handleChange}
                    />
                </div>
                <div className="mb-3">
                    <label htmlFor="user_id" className="form-label">User ID</label>
                    <input
                        type="text"
                        className="form-control"
                        id="user_Id"
                        name="user_Id"
                        value={add.user_Id}
                        onChange={handleChange}
                    />
                </div>
                <div className="mb-3">
                    <label htmlFor="food_id" className="form-label">Food ID</label>
                    <input
                        type="text"
                        className="form-control"
                        id="food_Id"
                        name="food_Id"
                        value={add.food_Id}
                        onChange={handleChange}
                    />
                </div>
                <div className="mb-3">
                    <label htmlFor="quantity" className="form-label">Quantity</label>
                    <input
                        type="text"
                        className="form-control"
                        id="quantity"
                        name="quantity"
                        value={add.quantity}
                        onChange={handleChange}
                    />
                </div>
                <Button 
                    type="submit"
                    className='fw-bold'>
                    Submit
                </Button>
            </form>
        </div>
    );
};

export default AddOrder;
