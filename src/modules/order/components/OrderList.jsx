import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useOrder } from '../core/hook'
import TableContainer from '../../../layout/table/components/TableContainer'
import { columns } from './table/column'

const OrderList = () => {
    const { list } = useSelector(state => state.order);
    const { getOrders } = useOrder();

    useEffect(() => {
        getOrders();
    }, []);

    return (
        <React.Fragment>
            <div className='bg-white mb-4 rounded-4 overflow-hidden'>
                <TableContainer title='Order List' columns={columns} data={list} pagination={true} />
            </div>
        </React.Fragment>
    )
}

export default OrderList