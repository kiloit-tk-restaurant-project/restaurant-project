import ActionCell from './ActionCell'

const columns = [
    {
        name: 'ID',
        selector: row => row.id,
    },
    {
        name: 'Table',
        selector: row => row.table.name,
    },
    {
        name: 'By',
        selector: row => row.user.name,
    },
    {
        name: 'Total Price',
        selector: row => `$ ${row.total_Price}`,
    },
    {
        name: 'Action',
        cell: row => <ActionCell id={row.id} />
    },
];

export { columns };