import React, { useState } from 'react'
import { IoMdMore } from 'react-icons/io'
import { FaEdit } from 'react-icons/fa'
import { FaEye } from 'react-icons/fa'
import OrderList from '../OrderList'

const ActionCell = ({id}) => {
    const [show, setShow] = useState(false);

    const showOffcanvas = () => setShow(true);
    const hideOffcanvas = () => setShow(false);

    return (
        <React.Fragment>
            <div className='dropdown'>
                <span className='dropdown-toggler d-flex justify-content-center align-items-center' data-bs-toggle='dropdown'>
                    <IoMdMore className='fs-5' />
                </span>
                <ul className='dropdown-menu'>
                    <li>
                        <a onClick={showOffcanvas} className='dropdown-item' role='button'><FaEye className='me-2 mb-1' />View details</a>4
                    </li>
                    <li>
                        <a onClick={undefined} className='dropdown-item' role='button'><FaEdit className='me-2 mb-1' />Update</a>
                    </li>
                </ul>
            </div>
            <OrderList show={show} onHide={hideOffcanvas} />
        </React.Fragment>
    )
}

export default ActionCell