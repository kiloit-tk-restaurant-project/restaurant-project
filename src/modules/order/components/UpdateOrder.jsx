// import React, { useEffect, useRef } from 'react';
// import { Form, Button } from 'react-bootstrap';
// import { Link, useParams } from 'react-router-dom';
// import { useDispatch, useSelector } from 'react-redux';
// import { changeOrderFields } from '../core/orderSlice';
// import { useOrder } from '../core/hook';
// import { IoArrowBack } from "react-icons/io5";

// const UpdateOrder = () => {
//     const focusInput = useRef(null);
//     const {list, order} = useSelector(state => state.order);
//     const dispatch = useDispatch();
//     const {id} = useParams();
//     const {getOrder, updateOrder} = useOrder();

//     useEffect(() => {
//         focusInput.current.focus();
//         getOrder();
//         const onUpdateOrder = list.find(order => order.id == id);
//         console.log(onUpdateOrder);
//         dispatch(changeOrderFields({...order, table_Id: onUpdateOrder.table.id, user_Id: onUpdateOrder.user.id}));
//     },[]);

//     const handleChange = e => {
//         dispatch(changeOrderFields({...order, [e.target.name]: e.target.value}));
//         console.log(order);
//     }
//     const handleSubmit = e => {
//         e.preventDefault();
//         const {table_Id, user_Id, food_Id, quantity} = order;
//         const payload = {
//             table_Id: parseInt(table_Id),
//             user_Id: parseInt(user_Id),
//             items: [
//                 {
//                     food_Id: food_Id,
//                     quantity: quantity
//                 }
//             ]
//         };
//         updateOrder(payload, id);
//     }

//     return (
//         <>
//             <div>
//                 <Link to='/order'>
//                     <Button className='mb-4 fw-bold'>
//                         <IoArrowBack />
//                     </Button>
//                 </Link>
//                 <h2 className='fw-bold'>Update Order</h2>
//             </div>
//             <Form onSubmit={handleSubmit}>
//                 <div className='mb-3'>
//                     <label htmlFor="table_Id" className="form-label">Table ID</label>
//                     <input
//                         ref={focusInput}
//                         className="form-control"
//                         type="text"
//                         name="table_Id"
//                         value={order.table_Id}
//                         onChange={handleChange}
//                     />
//                 </div>
//                 <div className='mb-3'>
//                     <label htmlFor="user_Id" className="form-label">User ID</label>
//                     <input
//                         className="form-control"
//                         type="text"
//                         name="user_Id"
//                         value={order.user_Id}
//                         onChange={handleChange}
//                     />
//                 </div>

//                 <div className='mb-3'>
//                     <label htmlFor="food_Id" className="form-label">Food ID</label>
//                     <input
//                         className="form-control"
//                         type="text"
//                         name="food_Id"
//                         value={order.food_Id}
//                         onChange={handleChange}
//                     />
//                 </div>
//                 <div className='mb-3'>
//                     <label htmlFor="quantity" className="form-label">Quantity</label>
//                     <input
//                         className="form-control"
//                         type="text"
//                         name="quantity"
//                         value={order.quantity}
//                         onChange={handleChange}
//                     />
//                 </div>
//                 <Button type="submit" className='fw-bold'>Submit</Button>
//             </Form>
//         </>
//     );
// };

// export default UpdateOrder;
