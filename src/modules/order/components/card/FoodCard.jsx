import React, { useState } from 'react'
import { Button, Card } from 'react-bootstrap'
import { IoAdd } from 'react-icons/io5'
import { useDispatch, useSelector } from 'react-redux'
import { fetchOrderItems } from '../../core/orderSlice'

const FoodCard = ({id, foodImage, name, description, price, discount}) => {
    const [quantity, setQuantity] = useState(1);
    const { orderItems } = useSelector(state => state.order);
    const dispatch = useDispatch();

    const handleIncrement = () => setQuantity(quantity + 1);
    const handleDecrement = () => quantity == 1 ? 1 : setQuantity(quantity - 1);
    
    const handleAdd = () => {
        const item = {food_id: id, foodImage, name, price, discount, quantity};
        const validationCheck = orderItems.some(({food_id}) => food_id === item.food_id);

        if (!validationCheck) {
            dispatch(fetchOrderItems([...orderItems, item]));
        } else {
            const updatedItems = orderItems.map(item => {
                if (item.food_id === id) {
                    return {...item, quantity};
                } else {
                    return item;
                }
            });
            discount(fetchOrderItems(updatedItems));
        }
    }

    return (
        <Card className='rounded-4 h-100'>
            <Card.Img className='rounded-0' src={foodImage || 'https://assets.epicurious.com/photos/5c745a108918ee7ab68daf79/master/pass/Smashburger-recipe-120219.jpg'} />
            <Card.Body>
                <Card.Title className='w-100'>{name}</Card.Title>
                <p className='w-100 card-text'>{description}</p>
                <p className='fw-bold d-flex gap-2 justify-content-between'>${Number(price).toFixed(2)}<span className='text-danger'>{Number(discount || 0).toFixed(2)}% OFF</span></p>
                <footer className='d-flex justify-content-between align-items-center gap-2'>
                    <div className='d-flex align-items-center gap-2'>
                        <a onClick={handleDecrement} role='button' className='btn-qty d-flex justify-content-center align-items-center rounded-circle text-decoration-none'>-</a>
                        <span>{quantity}</span>
                        <a onClick={handleIncrement} role='button' className='btn-qty d-flex justify-content-center align-items-center rounded-circle text-decoration-none'>+</a>
                    </div>
                    <Button onClick={handleAdd} className='rounded-3 d-flex align-items-center' variant='primary' size='sm'><IoAdd />Add</Button>
                </footer>
            </Card.Body>
        </Card>
    )
}

export default FoodCard