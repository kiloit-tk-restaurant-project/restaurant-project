import React, { useState } from 'react';
import axios from 'axios';
import { Form, Button, Alert } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { IoArrowBack } from "react-icons/io5";

const PaymentForm = () => {

    const [paymentAmount, setPaymentAmount] = useState('');
    const [paymentMethod, setPaymentMethod] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const [successMessage, setSuccessMessage] = useState('');

    const handlePaymentSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.put(`http://13.214.207.172:8001/api/orders/payment/6`, {
                amount: paymentAmount,
                method: paymentMethod
            });
            setSuccessMessage('Payment successful!');
            setErrorMessage('');

        } catch (error) {
            setErrorMessage('Error processing payment. Please try again.');
            setSuccessMessage('');
        }
    };
    const handleRefresh = () => {
        window.location.reload();
    };

    return (
        <>
            <div className='container'>
                <Link to='/order'>
                    <Button className='mb-4 text-center' variant='primary' type='submit'>
                        <IoArrowBack />
                    </Button>
                </Link>
            </div>
            <div className='bg-white p-3 rounded-4'>
                <h2 className='mb-3'>Make payment</h2>
                {errorMessage && <Alert variant='danger'>{errorMessage}</Alert>}
                {successMessage && <Alert variant='success'>{successMessage}</Alert>}
                <Form onSubmit={handlePaymentSubmit}>
                    <Form.Group controlId='paymentAmount'>
                        <Form.Label>Amount</Form.Label>
                        <Form.Control
                            type='number'
                            placeholder='Enter amount'
                            value={paymentAmount}
                            onChange={(e) => setPaymentAmount(e.target.value)}
                            required
                        />
                    </Form.Group>
                    <Form.Group controlId='paymentMethod' className='mt-3'>
                        <Form.Label>Payment Method</Form.Label>
                        <Form.Control
                            type='text'
                            placeholder='Enter payment method'
                            value={paymentMethod}
                            onChange={(e) => setPaymentMethod(e.target.value)}
                            required
                        />
                    </Form.Group>
                    <div className='d-flex gap-2 mt-4'>
                        <Button variant='primary' type='submit'>Submit</Button>
                        <Button variant='secondary' onClick={handleRefresh}>Canncel</Button>
                    </div>
                </Form>
            </div>
        </>
    );
};

export default PaymentForm;
