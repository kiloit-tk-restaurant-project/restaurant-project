import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import Order from './components/Order'
import OrderList from './components/OrderList'

const OrderPage = () => {
    return (
        <Routes>
            <Route index path='add-order' element={<Order />} />
            <Route path='list' element={<OrderList />} />
            <Route path='*' element={<Navigate to='ádd-order' />} />
        </Routes>
    )
}

export default OrderPage