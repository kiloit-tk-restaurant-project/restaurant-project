import { FaUser } from 'react-icons/fa'
import { FaUserCog } from 'react-icons/fa'
import { IoFastFood } from 'react-icons/io5'
import { GiChoice } from 'react-icons/gi'

const data = [
    {
        id: 1,
        subtitle: 'Users',
        icon: <FaUser className='h3' />,
        bg: 'primary',
        text: 'white',
    },
    {
        id: 2,
        subtitle: 'Roles',
        icon: <FaUserCog className='h3' />,
        bg: 'secondary',
        text: 'white',
    },
    {
        id: 3,
        subtitle: 'Foods',
        icon: <IoFastFood className='h3' />,
        bg: 'warning',
        text: 'white',
    },
    {
        id: 4,
        subtitle: 'Orders',
        icon: <GiChoice className='h3' />,
        bg: 'danger',
        text: 'white',
    },
];

export { data };