import React, { useEffect } from 'react'
import { Row } from 'react-bootstrap'
import { useSelector } from 'react-redux'
import { useUser } from '../../user/core/hook'
import { useRole } from '../../role/core/hook'
import { useFood } from '../../food/core/hook'
import { useOrder } from '../../order/core/hook'
import { data } from '../core/setup/data'
import OverviewCard from './card/OverviewCard'

const Dashboard = () => {
    const titles = [
        useSelector(state => state.user.list),
        useSelector(state => state.role.list),
        useSelector(state => state.food.list),
        useSelector(state => state.order.list),
    ];
    const { getUsers } = useUser();
    const { getRoles } = useRole();
    const { getFoods } = useFood();
    const { getOrders } = useOrder();

    useEffect(() => {
        getUsers();
        getRoles();
        getFoods();
        getOrders();
    }, []);

    return (
        <React.Fragment>
            <Row className='g-4' xs={1} md={2} lg={3} xl={4}>
                {data.map(props => {
                    return (
                        <OverviewCard key={props.id} {...props} title={titles[props.id - 1].length} />
                    )
                })}
            </Row>
        </React.Fragment>
    )
}

export default Dashboard