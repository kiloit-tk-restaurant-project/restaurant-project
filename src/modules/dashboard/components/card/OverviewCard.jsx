import React from 'react'
import { Card, Col } from 'react-bootstrap'

const OverviewCard = ({id, bg, text, title, icon, subtitle}) => {
    return (
        <Col key={id}>
            <Card bg={bg} text={text} className='border-0 h-100 rounded-4'>
                <Card.Body className='p-4 d-flex flex-column justify-content-between align-items-center gap-4'>
                    <Card.Title as='h1'>{title}</Card.Title>
                    <Card.Subtitle as='h3'>{subtitle} {icon}</Card.Subtitle>
                </Card.Body>
            </Card>
        </Col>
    )
}

export default OverviewCard