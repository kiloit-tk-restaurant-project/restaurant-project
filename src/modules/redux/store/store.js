import { configureStore } from '@reduxjs/toolkit'
import authSlice from '../../auth/core/authSlice'
import roleSlice from '../../role/core/roleSlice'
import orderSlice from '../../order/core/orderSlice'
import tableSlice from '../../table/core/tableSlice'
import userSlice from '../../user/core/userSlice'
import categorySlice from '../../foodCategory/core/categorySlice'
import foodSlice from '../../food/core/foodSlice'
import reportSlice from '../../report/core/reportSlice'
import sidebarSlice from '../../../layout/sidebar/core/handleOffcanvas'

export const store = configureStore({
    reducer: {
        sidebar: sidebarSlice,
        auth: authSlice,
        role: roleSlice,
        table: tableSlice,
        category: categorySlice,
        order: orderSlice,
        food: foodSlice,
        user: userSlice,
        report: reportSlice
    }
})