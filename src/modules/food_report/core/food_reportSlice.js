import { createSlice } from '@reduxjs/toolkit'

export const foodReportSlice = createSlice({
    
    name: 'foodReport',
    initialState: {
        list: [],
        foodReport: {},
    },
    reducers: {
        fetchFoodReport: (state, action) => {
            state.list = action.payload;
        },
        storeParams : (state, action) => {
            return {...state, params: {...state.params, ...action.payload}};
        },
    }
});

export const { fetchFoodReport, storeParams } = foodReportSlice.actions;
export default foodReportSlice.reducer;
