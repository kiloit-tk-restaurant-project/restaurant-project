import { useDispatch } from 'react-redux'
import { food_reportReq } from './request'
import { fetchFoodReport } from './food_reportSlice'
import { storeParams } from './food_reportSlice'
const useFoodReport = () => {
    const dispatch = useDispatch();

    const getFoodReport = () => {
        return food_reportReq().then(res => dispatch(fetchFoodReport(res.data.data))).catch(err => console.log(err));
    };
    const handleFilter = (name, value) => {
    dispatch(storeParams({ [name]: value }));
};
    return { getFoodReport, handleFilter };
}

export { useFoodReport };
