import React from 'react'
import { useEffect } from 'react';
import { useFoodReport } from '../core/hook';
import { useSelector } from 'react-redux';
import { Table } from 'react-bootstrap';

const Food_Report = () => {
    const { getFoodReport, handleFilter} = useFoodReport();
    const { list } = useSelector(state => state.food);
    // console.log(list);

    useEffect(() => {
        async function fetchData() {
            const response = await getFoodReport();
            console.log(response); 
        }
        fetchData();
    }, []);

    const collectParam = (key, value) => {
        const formattedValue = (key === 'start' || key === 'end') ? value.replace(/-/g, ':') : value;
        handleFilter(key, formattedValue);
    };

    return (
        <div>
            <div className='bg-white p-3 rounded-4'>
                <div className={`d-flex align-items-center reportDate border custom-border rounded-3 d-flex p-2 px-3`} style={{ height: '35px' }}>
                    <div className='d-flex text-nowrap'>
                        <label className='me-3 text-dark' htmlFor="">Start date</label>
                        <input
                            onChange={(e) => collectParam('start', e.target.value)}
                            className='form-control py-0 rounded me-3' type="date" name="" id=""
                        />
                    </div>
                    <div className='d-flex text-nowrap '>
                        <label className='me-3 text-dark' htmlFor="">End date</label>
                        <input
                            onChange={(e) => collectParam('end', e.target.value)}
                            className='form-control py-0 rounded' type="date" name="" id="" />
                    </div>
                </div>
                <div className='overflow-x-hidden'>
                    <div className='d-block'>
                        {list.length > 0 && (
                            <Table responsive>
                                <thead className=''>
                                    <tr className=''>
                                        <th scope="col" className='border-end'>ID</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Price per unit</th>
                                        <th scope="col">QuantitySold</th>
                                        <th scope="col">Total Price</th>
                                    </tr>
                                </thead>
                                <tbody className=''>
                                    {list.map((food, id) => (
                                        <tr key={id}>
                                            <td className='No'>{food?.food?.id}</td>
                                            <td className='Name'>{food?.food?.name}</td>
                                            <td className=''>
                                                <sup className='text-danger'>$</sup>
                                                {(food?.unitPrice).toFixed(2)}
                                            </td>
                                            <td className=''>{food?.totalQuantitySold} unit</td>
                                            <td className=''>
                                                <sup className='text-danger'>$</sup>
                                                {(food?.totalPrice).toFixed(2)}
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>
                        )}
                        {list.length === 0 && <p>No orders found!</p>}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Food_Report;
