import axios from 'axios'

const categoryGetReq = () => {
    return axios.get('/api/categories');
}
const categoryPostReq = (payload) => {
    return axios.post('/api/categories', payload);
}
const categoryPutReq = (id, payload) => {
    return axios.put(`/api/categories/${id}`, payload);
}
const categoryDeleteReq = (id) => {
    return axios.delete(`/api/categories/${id}`);
}
export { categoryGetReq, categoryPostReq, categoryPutReq, categoryDeleteReq };