import { useDispatch } from 'react-redux'
import { categoryGetReq, categoryPostReq, categoryPutReq, categoryDeleteReq } from './request'
import { fetchCategories } from './categorySlice'
import { useNavigate } from 'react-router-dom'

const useCategory = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const getCategories = () => {
        return categoryGetReq().then(res => {
            dispatch(fetchCategories(res.data.data));
        }).catch(err => console.log(err));
    }
    const postCategory = (payload) => {
        return categoryPostReq(payload).then(() => {
            alert('Added successfully!');
            navigate('/food-category');
        }).catch(err => {
            console.log(err);
        })
    }
    const putCategory = (id, payload) => {
        return categoryPutReq(id, payload).then(() => {
            alert('Updated successfully!');
            navigate('/food-category');
        }).catch(err => {
            console.log(err);
        })
    }
    const deleteCategory = (id) => {
        return categoryDeleteReq(id).then(() => {
            alert('Deleted successfully!');
            navigate('/food-category');
        }).catch(err => {
            console.log(err);
        })
    }

    return { getCategories, postCategory, putCategory, deleteCategory }
}

export { useCategory };