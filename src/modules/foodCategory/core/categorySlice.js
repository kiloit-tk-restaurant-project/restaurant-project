import { createSlice } from '@reduxjs/toolkit'

export const categorySlice = createSlice({
    name: 'category',
    initialState: {
        list: [],
        category: {
            id: '',
            name: '',
        },
        modal: false,
    },
    reducers: {
        fetchCategories: (state, action) => {
            state.list = action.payload;
        },
        changeCategoryFields: (state, action) => {
            const { id, name } = action.payload;
            state.category = { id, name };
        },
        clearCategoryFields: (state) => {
            state.category = { id: '', name: '' };
        },
        setModal: (state, action) => {
            state.modal = action.payload;
        }
    }
})

export const { fetchCategories, changeCategoryFields, clearCategoryFields, setModal } = categorySlice.actions;
export default categorySlice.reducer;