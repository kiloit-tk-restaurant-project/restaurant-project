import { Routes, Route, Navigate } from 'react-router-dom'
import FoodCategory from './components/FoodCategory'

const FoodCategoryPage = () => {
    return (
        <Routes>
            <Route index path='list' element={<FoodCategory />} />
            <Route path='*' element={<Navigate to='list'/>}/>
        </Routes>
    )
}

export default FoodCategoryPage