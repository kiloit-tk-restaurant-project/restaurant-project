import React, { createContext, useEffect, useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { useCategory } from '../core/hook'
import { useDispatch, useSelector } from 'react-redux'
import { changeCategoryFields, clearCategoryFields, setModal } from '../core/categorySlice'
import TableContainer from '../../../layout/table/components/TableContainer'
import { columns } from './table/column'
import { MdDelete } from 'react-icons/md'

export const CategoryContext = createContext();

const FoodCategory = () => {
    const { getCategories, postCategory, putCategory, deleteCategory } = useCategory();
    const {list, category, modal} = useSelector(state => state.category);
    const [addStatus, setAddStatus] = useState(true);
    const dispatch = useDispatch();
    
    const showModal = () => dispatch(setModal(true));
    const hideModal = () => dispatch(setModal(false));

    const handleChange = e => dispatch(changeCategoryFields({...category, [e.target.name]: e.target.value}));
    
    const handleClear = () => dispatch(clearCategoryFields());

    const showModalAdd = () => {
        handleClear();
        setAddStatus(true);
        showModal();
    }

    const handleSumit = e => {
        e.preventDefault();
        const {id, name} = category;
        addStatus ? postCategory({name}) : putCategory(id, {name});
        hideModal();
    }

    useEffect(() => {
        getCategories();
    }, []);

    const [selectedRows, setSelectedRows] = React.useState([]);
    
    const handleRowSelected = React.useCallback(state => {
		setSelectedRows(state.selectedRows);
	}, []);

    const contextActions = React.useMemo(() => {
        const handleDelete = () => {
            if (window.confirm('Are you sure to delete?')) {
                deleteCategory(selectedRows.map(row => row.id));
            }
        }

		return (
            <Button onClick={handleDelete} className='rounded-3' variant='danger' size='sm'><MdDelete className='me-1 mb-1' /> Delete</Button>
		);
	}, [selectedRows, list]);

    return (
        <CategoryContext.Provider value={{list, category, setAddStatus}}>
            <React.Fragment>
                <div className='mb-4'>
                    <Button onClick={showModalAdd} variant='primary' className='rounded-3'>Add Category</Button>
                </div>
                <div className='bg-white mb-4 rounded-4 overflow-hidden'>
                    <TableContainer title='Food Category List' columns={columns} data={list} handleRowSelected={handleRowSelected} contextActions={contextActions} pagination={true} />
                </div>
                <Modal show={modal} onHide={hideModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>{addStatus ? 'New' : 'Udpate'} Category</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group controlId='name'>
                                <Form.Label className='mb-1'>Name</Form.Label>
                                <Form.Control value={category.name} onChange={handleChange} name='name' className='rounded-3' type='text' />
                            </Form.Group>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={handleSumit} variant='primary' className='rounded-3'>{addStatus ? 'Add' : 'Update'}</Button>
                        <Button onClick={handleClear} variant='secondary' className='rounded-3'>Reset</Button>
                    </Modal.Footer>
                </Modal>
            </React.Fragment>
        </CategoryContext.Provider>
    )
}

export default FoodCategory