import ActionCell from './ActionCell'

const columns = [
    {
        name: 'Name',
        selector: row => row.name,
    },
    {
        name: 'Created By',
        selector: row => row.createdBy.username,
    },
    {
        name: 'Action',
        selector: row => row.action,
        cell: row => <ActionCell id={row.id} />
    },
];

export { columns };