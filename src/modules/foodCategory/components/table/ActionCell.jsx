import React, { useContext } from 'react'
import { IoMdMore } from 'react-icons/io'
import { FaEdit } from 'react-icons/fa'
import { useDispatch } from 'react-redux'
import { changeCategoryFields, setModal } from '../../core/categorySlice'
import { CategoryContext } from '../FoodCategory'

const ActionCell = ({id}) => {
    const {list, category, setAddStatus} = useContext(CategoryContext);
    const dispatch = useDispatch();

    const onUpdate = () => {
        setAddStatus(false);
        dispatch(setModal(true));
        const {name} = list.find(category => category.id === id);
        dispatch(changeCategoryFields({...category, id, name}));
    };

    return (
        <div className='dropdown'>
            <span className='dropdown-toggler d-flex justify-content-center align-items-center' data-bs-toggle='dropdown'>
                <IoMdMore className='fs-5' />
            </span>
            <ul className='dropdown-menu'>
                <li>
                    <a onClick={onUpdate} className='dropdown-item' role='button'><FaEdit className='me-1 mb-1' />Update</a>
                </li>
            </ul>
        </div>
    )
}

export default ActionCell