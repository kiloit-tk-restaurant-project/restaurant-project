import React from 'react';

const Pagination = ({ currentPage, itemsPerPage, totalItems, onPageChange }) => {
    const totalPages = Math.ceil(totalItems / itemsPerPage);
    const handlePageChange = (pageNumber) => {
        onPageChange(pageNumber);
    };

    const renderPageNumbers = () => {
        const pageNumbers = [];
        for (let i = 1; i <= totalPages; i++) {
            pageNumbers.push(
                <li key={i} className={currentPage === i ? 'page-item active' : 'page-item'}>
                    <a className="page-link text-primary" onClick={() => handlePageChange(i)}>{i}</a>
                </li>
            );
        }
        return pageNumbers;
    };

    const handlePrevClick = () => {
        window.location.reload();
    };

    const handleNextClick = () => {
        if (currentPage < totalPages) {
            onPageChange(currentPage + 1);
        }
    };

    return (
        <div>
            <nav aria-label="Page navigation example">
                <ul className="text-primary pagination">
                    <li className={currentPage === 1 ? 'page-item disabled ' : 'page-item '}>
                        <a className="page-link text-primary" role='button' onClick={handlePrevClick}>Previous</a>
                    </li>
                    {renderPageNumbers()}
                    <li className={currentPage === totalPages ? 'page-item disabled' : 'page-item'}>
                        <a className="page-link text-primary" role='button' onClick={handleNextClick}>Next</a>
                    </li>
                </ul>
            </nav>
        </div>
    );
};

export default Pagination;
